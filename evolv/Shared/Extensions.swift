//
//  Extensions.swift
//  evolv
//
//  Created by Nicholas Trampe on 7/18/18.
//  Copyright © 2018 Nicholas Trampe. All rights reserved.
//

import SpriteKit

extension CGVector {
  public static func * (vector: CGVector, scalar: CGFloat) -> CGVector {
    return CGVector(dx: vector.dx * scalar, dy: vector.dy * scalar)
  }
}


extension SKAction {
  class func flash(duration: TimeInterval) -> SKAction {
    return SKAction.sequence([SKAction.fadeOut(withDuration: duration / 3.0), SKAction.wait(forDuration: duration / 3.0), SKAction.fadeIn(withDuration: duration / 3.0)])
  }
  
  class func oscillate(in direction: Direction, distances: [CGFloat], weights: [Double], duration: TimeInterval) -> SKAction {
    precondition(distances.count > 0, "Distances count must be greater than zero")
    precondition(weights.count == distances.count, "Must have same number of weights and distances")
    
    var moves: [SKAction] = []
    
    for (index, distance) in distances.enumerated() {
      let move = SKAction.move(by: direction.vector * distance, duration: duration * weights[index])
      moves.append(move)
    }
    
    return SKAction.sequence(moves)
  }
}


// TODO: Implement this in 4.2
//https://stackoverflow.com/questions/24026510/how-do-i-shuffle-an-array-in-swift

extension MutableCollection {
  /// Shuffles the contents of this collection.
  mutating func shuffle() {
    let c = count
    guard c > 1 else { return }
    
    for (firstUnshuffled, unshuffledCount) in zip(indices, stride(from: c, to: 1, by: -1)) {
      // Change `Int` in the next line to `IndexDistance` in < Swift 4.1
      let d: Int = numericCast(arc4random_uniform(numericCast(unshuffledCount)))
      let i = index(firstUnshuffled, offsetBy: d)
      swapAt(firstUnshuffled, i)
    }
  }
}

extension Sequence {
  /// Returns an array with the contents of this sequence, shuffled.
  func shuffled() -> [Element] {
    var result = Array(self)
    result.shuffle()
    return result
  }
}
