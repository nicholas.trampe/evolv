//
//  TextureAtlasHelper.swift
//  evolv
//
//  Created by Nicholas Trampe on 7/18/18.
//  Copyright © 2018 Nicholas Trampe. All rights reserved.
//

import SpriteKit

func texturesInAtlas(named: String, starting: Int = 1, prefix: String? = nil, suffix: String? = nil, reversed: Bool = false) -> [SKTexture] {
  let atlas = SKTextureAtlas(named: named)
  var textures: [SKTexture] = []
  
  for i in starting...atlas.textureNames.count {
    let name = "\(prefix ?? "")\(i)\(suffix ?? "")"
    let tex = atlas.textureNamed(name)
    
    if reversed {
      textures.insert(tex, at: 0)
    } else {
      textures.append(tex)
    }
  }
  
  return textures
}
