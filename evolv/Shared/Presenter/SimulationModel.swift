//
//  SimulationModel.swift
//  evolv iOS
//
//  Created by Nicholas Trampe on 7/17/18.
//  Copyright © 2018 Nicholas Trampe. All rights reserved.
//

import Foundation

enum PopulationID: UInt8 {
  case none = 0
  case pip = 1
  case chip = 2
  case flip = 4
}

struct PopulationIDMask: OptionSet {
  let rawValue: UInt8
  
  static let none = PopulationIDMask(rawValue: PopulationID.none.rawValue)
  static let pip = PopulationIDMask(rawValue: PopulationID.pip.rawValue)
  static let chip = PopulationIDMask(rawValue: PopulationID.chip.rawValue)
  static let flip = PopulationIDMask(rawValue: PopulationID.flip.rawValue)
  
  static let all: PopulationIDMask = [.pip, .chip, .flip]
}

protocol SimulationModelObserver: AnyObject {
  func didAdd(organism: Organism)
  func didRemove(organism: Organism)
  func didAdd(item: Item)
  
  func didPerform(action: Action, result: ActionResult)
  
  func didStartTrackingOrganism(organism: Organism)
  func didTrack(organism: Organism)
  func didStopTrackingOrganism(organism: Organism)
  
  func didUpdate(time: UInt)
  func didUpdate(metrics: SimulationMetrics)
  func didUpdate(interval: Double)
  func didFinishSimulation()
}

protocol SimulationModel {
  var observer: SimulationModelObserver? { get set }
  
  var size: Size { get }
  var updateInterval: Double { get }
  
  var isRunning: Bool { get }
  var isTracking: Bool { get }
  
  func setUpdateInterval(_ interval: Double)
  func start()
  func pause()
  func resume()
  func fastForward()
  
  func startTracking(organism: Organism)
  func stopTracking()
  func organism(at location: Point) -> Organism?
}
