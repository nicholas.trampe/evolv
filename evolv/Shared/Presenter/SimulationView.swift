//
//  SimulationView.swift
//  evolv iOS
//
//  Created by Nicholas Trampe on 7/17/18.
//  Copyright © 2018 Nicholas Trampe. All rights reserved.
//

import SpriteKit

struct NodeID: Hashable {
  let category: UInt32
  let subcategory: UInt32
  
  init(_ category: UInt32, _ subcategory: UInt32) {
    self.category = category
    self.subcategory = subcategory
  }
}

protocol SimulationViewObserver: AnyObject {
  func didBeginPanning(at position: CGPoint)
  func didPan(distance: CGPoint)
  func didBeginZooming(at scale: CGFloat)
  func didZoom(scale: CGFloat)
  func didZoom(delta: CGFloat)
  func didTap(point: CGPoint)
  
  func didPressControl(at index: Int)
}

protocol SimulationView {
  var observer: SimulationViewObserver? { get set }
  var animationInterval: Double { get set }
  
  var cameraPosition: CGPoint { get }
  var cameraScale: CGFloat { get }
  var size: CGSize { get }
  
  //// Adding / Removing Nodes
  
  func addNode(id: NodeID, position: CGPoint, zPosition: CGFloat, texture: SKTexture, scale: CGFloat)
  func removeNode(id: NodeID)
  func removeAllNodes()
  
  //// Accessors
  
  func location(for id: NodeID) -> CGPoint?
  func xScale(for id: NodeID) -> CGFloat?
  func yScale(for id: NodeID) -> CGFloat?
  
  //// Animations for Nodes
  
  func animateNode(id: NodeID, action: SKAction)
  func animateNode(id: NodeID, action: SKAction, completion block: @escaping () -> Void)
  
  func animateNode(id: NodeID, textures: [SKTexture])
  func animateNode(id: NodeID, textures: [SKTexture], completion block: @escaping () -> Void)
  
  func moveNode(id: NodeID, position: CGPoint)
  func moveNode(id: NodeID, position: CGPoint, completion block: @escaping () -> Void)
  
  func scaleNode(id: NodeID, scaleX: CGFloat, scaleY: CGFloat, animated: Bool)
  func scaleNode(id: NodeID, scaleX: CGFloat, scaleY: CGFloat, animated: Bool, completion block: @escaping () -> Void)
  
  func flashNode(id: NodeID)
  func flashNode(id: NodeID, completion block: @escaping () -> Void)
  
  func oscillateNode(id: NodeID, in direction: Direction, distances: [CGFloat], weights: [Double])
  func oscillateNode(id: NodeID, in direction: Direction, distances: [CGFloat], weights: [Double], completion block: @escaping () -> Void)
  
  //// Animations for Camera
  
  func move(to position: CGPoint, duration: Double)
  func zoom(to scale: CGFloat, duration: Double)
  
  //// Debug View
  
  func setText(id: UInt32, text: String?, at position: CGPoint)
  func removeText(id: UInt32)
  
  //// Control View
  
  func addControl(icon: IconFontIcon, at index: Int)
  func updateControl(icon: IconFontIcon, at index: Int)

  //// Tile Background View
  
  func addTileMap(id: UInt32, named: String, columns: Int, rows: Int, tileSize: CGSize)
  func setTileFill(id: UInt32, named: String)
  func setTileFill(id: UInt32, named: String, column: Int, row: Int)
  func clearTiles(id: UInt32)
}
