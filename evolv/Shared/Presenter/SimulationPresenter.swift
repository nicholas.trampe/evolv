//
//  SimulationPresenter.swift
//  evolv iOS
//
//  Created by Nicholas Trampe on 7/17/18.
//  Copyright © 2018 Nicholas Trampe. All rights reserved.
//

import SpriteKit

protocol SimulationPresenter {
  func start()
}

class EVSimulationPresenter: SimulationPresenter {
  private var view: SimulationView
  private var model: SimulationModel
  private let scale: Double = 128
  private var previousCameraLocation: CGPoint = .zero
  private var previousCameraScale: CGFloat = 0
  private lazy var pipJumpTextures = texturesInAtlas(named: "PipJump", starting: 1, prefix: "pip_jump")
  private lazy var pipBlinkTextures = texturesInAtlas(named: "PipBlink", starting: 1, prefix: "pip_blink")
  private lazy var pipDieTextures = texturesInAtlas(named: "PipDie", starting: 1, prefix: "pip_die")
  private lazy var pipAttackTextures = texturesInAtlas(named: "PipAttack", starting: 1, prefix: "pip_attack")
  private lazy var pipEatTextures = texturesInAtlas(named: "PipEat", starting: 1, prefix: "pip_eat")
  private lazy var chipJumpTextures = texturesInAtlas(named: "ChipJump", starting: 1, prefix: "chip_jump")
  private lazy var chipBlinkTextures = texturesInAtlas(named: "ChipBlink", starting: 1, prefix: "chip_blink")
  private lazy var chipDieTextures = texturesInAtlas(named: "ChipDie", starting: 1, prefix: "chip_die")
  private lazy var chipAttackTextures = texturesInAtlas(named: "ChipAttack", starting: 1, prefix: "chip_attack")
  private lazy var chipEatTextures = texturesInAtlas(named: "ChipEat", starting: 1, prefix: "chip_eat")
  private lazy var flipJumpTextures = texturesInAtlas(named: "FlipJump", starting: 1, prefix: "flip_jump")
  private lazy var flipBlinkTextures = texturesInAtlas(named: "FlipBlink", starting: 1, prefix: "flip_blink")
  private lazy var flipDieTextures = texturesInAtlas(named: "FlipDie", starting: 1, prefix: "flip_die")
  private lazy var flipAttackTextures = texturesInAtlas(named: "FlipAttack", starting: 1, prefix: "flip_attack")
  private lazy var flipEatTextures = texturesInAtlas(named: "FlipEat", starting: 1, prefix: "flip_eat")
  private lazy var foodEatTextures = texturesInAtlas(named: "FoodEat", starting: 1, prefix: "food")
  
  init(view: SimulationView, model: SimulationModel) {
    self.view = view
    self.model = model
    
    self.view.observer = self
    self.model.observer = self
    
    self.view.addControl(icon: .pause, at: 0)
    self.view.addControl(icon: .forward, at: 1)
  }
  
  func start() {
    view.addTileMap(id: 0,
                    named: "Sample Grid Tile Set",
                    columns: Int(model.size.width),
                    rows: Int(model.size.height),
                    tileSize: CGSize(width: CGFloat(scale), height: CGFloat(scale)))
    view.addTileMap(id: 1,
                    named: "Sample Grid Tile Set",
                    columns: Int(model.size.width),
                    rows: Int(model.size.height),
                    tileSize: CGSize(width: CGFloat(scale), height: CGFloat(scale)))
    view.setTileFill(id: 0, named: "Grass")
    
    resetCamera(duration: 0)
    
    model.setUpdateInterval(1.0 / 1.0)
    model.start()
  }
  
  private func resetCamera(duration: Double) {
    let modelSize: Double = model.size.width * scale
    let zoom = CGFloat(modelSize) / view.size.width * 2.0
    view.zoom(to: zoom, duration: duration)
    view.move(to: convertToView(point: Point(model.size.half.width, model.size.half.height)), duration: duration)
  }
}


extension EVSimulationPresenter: SimulationModelObserver {
  func didAdd(organism: Organism) {
    view.addNode(id: convertToID(organism),
                 position: convertToView(point: organism.location),
                 zPosition: 1,
                 texture: jumpTextures(for: organism).first!,
                 scale: scale(for: organism.energy))
  }
  
  func didRemove(organism: Organism) {
    view.animateNode(id: convertToID(organism), textures: dieTextures(for: organism)) {
      self.view.removeNode(id: self.convertToID(organism))
    }
  }
  
  func didAdd(item: Item) {
    switch item.type {
    case .food:
      view.addNode(id: convertToID(item),
                   position: convertToView(point: item.location),
                   zPosition: -1,
                   texture: foodEatTextures.first!,
                   scale: 1.0)
    case .water:
      view.setTileFill(id: 1, named: "Water", column: Int(item.location.x), row: Int(item.location.y))
    case .wall:
      view.setTileFill(id: 1, named: "Cobblestone", column: Int(item.location.x), row: Int(item.location.y))
    }
  }
  
  func didPerform(action: Action, result: ActionResult) {
    switch action {
    case .move(_,_):
      move(organism: result.actingOrganism, in: action.direction)
    case .attack(_,_):
      attack(predator: result.affectedOrganisms[0], prey: result.affectedOrganisms[1])
    case .eat(_,_):
      eat(food: result.affectedItems[0], organism: result.actingOrganism, in: action.direction)
    case .drink(_,_):
      drink(water: result.affectedItems[0], organism: result.actingOrganism, in: action.direction)
    case .rest(_):
      rest(organism: result.actingOrganism)
    }
  }
  
  func didStartTrackingOrganism(organism: Organism) {
    view.move(to: convertToView(point: organism.location), duration: 0.3)
    
    view.setText(id: 2, text: "Tracking Organism:\n" + organism.description, at: convertToView(point: Point(organism.location.x, organism.location.y+1)))
  }
  
  func didTrack(organism: Organism) {
    view.move(to: convertToView(point: organism.location), duration: view.animationInterval)
    
    view.setText(id: 2, text: "Tracking Organism:\n" + organism.description, at: convertToView(point: Point(organism.location.x, organism.location.y+1)))
  }
  
  func didStopTrackingOrganism(organism: Organism) {
    view.removeText(id: 2)
  }
  
  func didUpdate(time: UInt) {
    
  }
  
  func didUpdate(metrics: SimulationMetrics) {
    view.removeAllNodes()
    view.clearTiles(id: 1)
    
    view.setText(id: 1, text: metrics.description, at: convertToView(point: Point(model.size.width, 0)))
  }
  
  func didUpdate(interval: Double) {
    view.animationInterval = interval * 0.5
  }
  
  func didFinishSimulation() {
    
  }
  
  // Private API
  
  private func move(organism: Organism, in direction: Direction) {
    view.moveNode(id: convertToID(organism), position: convertToView(point: organism.location))
    view.animateNode(id: convertToID(organism), textures: jumpTextures(for: organism))
    scale(organism: organism, for: direction)
  }
  
  private func eat(food: Item, organism: Organism, in direction: Direction) {
    view.animateNode(id: convertToID(food), textures: foodEatTextures) {
      self.view.removeNode(id: self.convertToID(food))
    }
    
    view.animateNode(id: convertToID(organism), textures: eatTextures(for: organism))
    view.moveNode(id: convertToID(organism), position: convertToView(point: food.location))
    scale(organism: organism, for: direction)
  }
  
  private func drink(water: Item, organism: Organism, in direction: Direction) {
    let distance: CGFloat = CGFloat(scale) * 0.25
    let distances: [CGFloat] = [distance, -distance]
    let weights: [Double] = [0.5, 0.5]
    
    view.oscillateNode(id: convertToID(organism), in: direction, distances: distances, weights: weights)
    
    view.animateNode(id: convertToID(organism), textures: eatTextures(for: organism))
    scale(organism: organism, for: direction)
  }
  
  private func attack(predator: Organism, prey: Organism) {
    let direction = predator.location.direction(to: prey.location)
    let distance: CGFloat = CGFloat(scale) * 0.75
    let distances: [CGFloat] = [(-distance / 2.0), (distance * 3 / 2), -distance]
    let weights: [Double] = [0.5, 0.25, 0.25]
    
    scale(organism: predator, for: predator.location.direction(to: prey.location))
    view.animateNode(id: convertToID(predator), textures: attackTextures(for: predator)) {}
    
    view.oscillateNode(id: convertToID(predator), in: direction, distances: distances, weights: weights) {
      self.view.flashNode(id: self.convertToID(prey))
    }
  }
  
  private func rest(organism: Organism) {
    view.animateNode(id: convertToID(organism), textures: blinkTextures(for: organism))
    scale(organism: organism)
  }
  
  private func convertToView(point: Point) -> CGPoint {
    return CGPoint(x: CGFloat(point.x*scale), y: CGFloat(point.y*scale))
  }
  
  private func convertToModel(point: CGPoint) -> Point {
    let convertedX = Int(Double(point.x)/scale)
    let convertedY = Int(Double(point.y)/scale)
    return Point(Double(convertedX), Double(convertedY))
  }
  
  private func convertToID(_ organism: Organism) -> NodeID {
    return NodeID(organism.id, UInt32(organism.populationID.rawValue))
  }
  
  private func convertToID(_ item: Item) -> NodeID {
    return NodeID(item.id, 99)
  }
  
  private func fillColor(for energy: Float) -> SKColor {
    return SKColor(red: min(CGFloat(energy) / 50.0, 1.0), green: 0.0, blue: min(10.0 / CGFloat(energy), 1.0), alpha: 1.0)
  }
  
  private func size(for energy: Float) -> CGSize {
    let s = CGFloat(energy / 10.0) * CGFloat(scale) / 2
    return CGSize(width: s, height: s)
  }
  
  private func scale(for energy: UInt) -> CGFloat {
    return 1.0
  }
  
  private func scale(organism: Organism, for direction: Direction) {
    guard let xScale = view.xScale(for: convertToID(organism)) else {
      return
    }
    
    let lefts: [Direction] = [.west, .northwest, .southwest]
    let rights: [Direction] = [.east, .northeast, .southeast]
    var multiplier: CGFloat = 1.0
    
    if lefts.contains(direction) {
      multiplier = -1.0
    } else if rights.contains(direction) {
      multiplier = 1.0
    } else {
      let wasLeft = xScale < 0
      
      multiplier = wasLeft ? -1.0 : 1.0
    }
    
    let updatedScale = scale(for: organism.energy) * multiplier
    
    view.scaleNode(id: convertToID(organism), scaleX: updatedScale, scaleY: scale(for: organism.energy), animated: false)
  }
  
  private func scale(organism: Organism) {
    guard let xScale = view.xScale(for: convertToID(organism)) else {
      return
    }
    
    let wasLeft = xScale < 0
    let multiplier: CGFloat = wasLeft ? -1.0 : 1.0
    
    let updatedScale = scale(for: organism.energy) * multiplier
    
    view.scaleNode(id: convertToID(organism), scaleX: updatedScale, scaleY: scale(for: organism.energy), animated: false)
  }
  
  private func jumpTextures(for organism: Organism) -> [SKTexture] {
    switch organism.populationID {
    case .pip:
      return pipJumpTextures
    case .chip:
      return chipJumpTextures
    case .flip:
      return flipJumpTextures
    default:
      return []
    }
  }
  
  private func blinkTextures(for organism: Organism) -> [SKTexture] {
    switch organism.populationID {
    case .pip:
      return pipBlinkTextures
    case .chip:
      return chipBlinkTextures
    case .flip:
      return flipBlinkTextures
    default:
      return []
    }
  }
  
  private func eatTextures(for organism: Organism) -> [SKTexture] {
    switch organism.populationID {
    case .pip:
      return pipEatTextures
    case .chip:
      return chipEatTextures
    case .flip:
      return flipEatTextures
    default:
      return []
    }
  }
  
  private func attackTextures(for organism: Organism) -> [SKTexture] {
    switch organism.populationID {
    case .pip:
      return pipAttackTextures
    case .chip:
      return chipAttackTextures
    case .flip:
      return flipAttackTextures
    default:
      return []
    }
  }
  
  private func dieTextures(for organism: Organism) -> [SKTexture] {
    switch organism.populationID {
    case .pip:
      return pipDieTextures
    case .chip:
      return chipDieTextures
    case .flip:
      return flipDieTextures
    default:
      return []
    }
  }
}

extension EVSimulationPresenter: SimulationViewObserver {
  func didBeginPanning(at position: CGPoint) {
    previousCameraLocation = position
  }
  
  func didPan(distance: CGPoint) {
    
    if model.isTracking {
      model.stopTracking()
    }
    
    let position = CGPoint(x: previousCameraLocation.x - distance.x * view.cameraScale, y: previousCameraLocation.y - distance.y * view.cameraScale)
    view.move(to: position, duration: 0)
  }
  
  func didBeginZooming(at scale: CGFloat) {
    previousCameraScale = scale
  }
  
  func didZoom(scale: CGFloat) {
    let scale = previousCameraScale * 1 / scale
    view.zoom(to: scale, duration: 0)
  }
  
  func didZoom(delta: CGFloat) {
    let previousScale = view.cameraScale
    let newScale = CGFloat(fabsf(Float(previousScale - delta / 100)))
    view.zoom(to: newScale, duration: 0)
  }
  
  func didTap(point: CGPoint) {
    let location = convertToModel(point: point)
    if let organism = model.organism(at: location) {
      model.startTracking(organism: organism)
    } else {
      model.stopTracking()
      resetCamera(duration: 0.3)
    }
  }
  
  func didPressControl(at index: Int) {
    switch index {
    case 0:
      playPause()
    case 1:
      model.fastForward()
    default:
      break
    }
  }
  
  private func playPause() {
    if model.isRunning {
      model.pause()
      view.updateControl(icon: .play, at: 0)
    } else {
      model.resume()
      view.updateControl(icon: .pause, at: 0)
    }
  }
}
