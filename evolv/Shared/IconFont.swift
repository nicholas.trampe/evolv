//
//  IconFont.swift
//  blog
//
//  Created by Nicholas Trampe on 8/20/17.
//  Copyright © 2017 Nicholas Trampe. All rights reserved.
//

#if os(OSX)
import AppKit
#elseif os(iOS)
import UIKit
#endif

enum IconFontIcon: String {
  case play = "\u{ea1c}"
  case pause = "\u{ea1d}"
  case stop = "\u{ea1e}"
  case backward = "\u{ea1f}"
  case forward = "\u{ea20}"
  case first = "\u{ea21}"
  case last = "\u{ea22}"
  case previous = "\u{ea23}"
  case next = "\u{ea24}"
}

extension Font {
  static func iconFont(of size: CGFloat) -> Font? {
    return Font(name: "icomoon", size: size)
  }
}

extension Label {
  static func iconLabel(of size: CGFloat, with icon: IconFontIcon) -> Label {
    let label = Label()
    label.translatesAutoresizingMaskIntoConstraints = false
    label.textAlignment = .center
    label.font = Font.iconFont(of: size)
    label.text = icon.rawValue
    return label
  }
  
  var icon: IconFontIcon? {
    get {
      if let text = self.text {
        return IconFontIcon(rawValue: text)
      }
      return nil
    }
    set {
      self.text = newValue?.rawValue
    }
  }
}

extension Button {
  #if os(iOS)
  static func iconButton(of size: CGFloat, with icon: IconFontIcon) -> Button {
    let button = Button(frame: .zero)
    button.translatesAutoresizingMaskIntoConstraints = false
    button.title = icon.rawValue
    button.font = Font.iconFont(of: size)
    
    return button
  }
  #else
  static func iconButton(of size: CGFloat, with icon: IconFontIcon) -> Button {
    let button = IconButton(frame: .zero)
    button.translatesAutoresizingMaskIntoConstraints = false
    button.title = icon.rawValue
    button.font = Font.iconFont(of: size)
    return button
  }
  #endif
  
  var icon: IconFontIcon? {
    get {
      return IconFontIcon(rawValue: self.title)
    }
    set {
      self.title = newValue?.rawValue ?? ""
    }
  }
}
