//
//  PopulationConfig.swift
//  evolv iOS
//
//  Created by Nicholas Trampe on 7/20/18.
//  Copyright © 2018 Nicholas Trampe. All rights reserved.
//

import Foundation

enum PopulationTerminationMethod {
  case numberOfEvaluationsReached
  case bestFitnessConverged
}

enum PopulationInitializationType {
  case random
  case rampedHalfAndHalf
}

enum PopulationParentSelectionType {
  case fitnessProportional
  case overSelection
}

enum PopulationSurvivalSelectionType {
  case truncation
  case tournamentWithoutReplacement
}

enum PopulationCrossOverType {
  case subtree
}

enum PopulationMutationType {
  case none
  case subtree
}

enum PopulationBloatPreventionMethodType {
  case none
  case parsimonyPressure
}

struct PopulationConfig {
  let populationSize: UInt
  let offspringSize: UInt
  let maxDepth: UInt
  
  let terminals: [OrganismTerminal]
  
  let attackMask: PopulationIDMask
  let canEatFood: Bool
  
  let initializationType: PopulationInitializationType
  let parentSelectionType: PopulationParentSelectionType
  let survivalSelectionType: PopulationSurvivalSelectionType
  let crossOverType: PopulationCrossOverType
  let mutationType: PopulationMutationType
  let bloatPreventionMethodType: PopulationBloatPreventionMethodType
  let preventSimpleSolutions: Bool
  
  let survivalSelectionTournamentSize: UInt
  let mutationSubTreeDepth: UInt
  let parsimonyPressureCoefficient: Double
}


