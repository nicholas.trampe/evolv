//
//  ParseTree.swift
//  evolv iOS
//
//  Created by Nicholas Trampe on 7/10/18.
//  Copyright © 2018 Nicholas Trampe. All rights reserved.
//

import Foundation

enum ParseTreeOperator {
  case none
  case addition
  case subtraction
  case multiplication
  case max
  case min
  case division
  case random
  case `if`
  case and
  case or
  case terminal(id: String)
}

extension ParseTreeOperator: CustomStringConvertible {
  var description: String {
    get {
      switch self {
      case .none:
        return "?"
      case .addition:
        return "+"
      case .subtraction:
        return "-"
      case .multiplication:
        return "*"
      case .division:
        return "/"
      case .max:
        return "max"
      case .min:
        return "min"
      case .random:
        return "r"
      case .if:
        return "if"
      case .and:
        return "and"
      case .or:
        return "or"
      case .terminal(let  id):
        return id
      }
    }
  }
}

extension ParseTreeOperator {
  static var functionalOperators: [ParseTreeOperator] = [.random,
                                                         .addition,
                                                         .subtraction,
                                                         .multiplication,
                                                         .division,
                                                         .max,
                                                         .min,
                                                         .if,
                                                         .and,
                                                         .or]
  
  var isTerminal: Bool {
    get {
      switch self {
      case .terminal(_):
        return true
      default:
        return false
      }
    }
  }
}


class ParseTreeNode {
  var `operator`: ParseTreeOperator = .none
  
  var left: ParseTreeNode? = nil
  var right: ParseTreeNode? = nil
  var parent: ParseTreeNode? = nil
  
  convenience init(`operator`: ParseTreeOperator) {
    self.init()
    self.operator = `operator`
  }
  
  func clone() -> ParseTreeNode {
    return ParseTreeNode(operator: self.operator)
  }
}

enum ParseTreeType {
  case random(depth: UInt)
  case rampedHalfAndHalf(depth: UInt)
}

class ParseTree {
  private var nullNode: ParseTreeNode? = nil
  private(set) var root: ParseTreeNode? = nil
  
  let terminals: [String]
  
  init(terminals: [String]) {
    self.terminals = terminals
  }
  
  init(type: ParseTreeType, terminals: [String]) {
    self.terminals = terminals
    
    switch type {
    case .random(let depth):
      createTreeRandomly(maxDepth: depth)
    case .rampedHalfAndHalf(let depth):
      createTreeWithRampedHalfAndHalf(maxDepth: depth)
    }
  }
  
  init(tree: ParseTree) {
    terminals = tree.terminals
    copyNodes(startingAt: &root, copy: tree.root, parent: nullNode)
  }
  
  // Public API
  
  var size: Int {
    get {
      return size(startingAt: root)
    }
  }
  
  //// Fitness
  
  func value(_ operate: (String) -> (Double)) -> Double {
    return value(startingAt: root, operate)
  }
  
  //// Recombination and Mutation
  
  func mutate(subtreeDepth: UInt) {
    var node = randomNode()
    let tree = ParseTree(type: .rampedHalfAndHalf(depth: subtreeDepth), terminals: terminals)
    
    insert(tree: tree, at: &node)
  }
  
  static func recombine(tree1: ParseTree, tree2: ParseTree) -> ParseTree {
    if arc4random() % 2 == 0 {
      let tree = ParseTree(tree: tree1)
      var node1 = tree.randomNode()
      let node2 = tree2.randomNode()
      let subtree = tree2.subtree(startingAt: node2)
      
      tree.insert(tree: subtree, at: &node1)
      return tree
    } else {
      let tree = ParseTree(tree: tree2)
      let node1 = tree1.randomNode()
      var node2 = tree.randomNode()
      let subtree = tree1.subtree(startingAt: node1)
      
      tree.insert(tree: subtree, at: &node2)
      return tree
    }
  }
  
  func subtree(startingAt node: ParseTreeNode?) -> ParseTree {
    let tree = ParseTree(terminals: terminals)
    copyNodes(startingAt: &tree.root, copy: node, parent: nullNode)
    return tree
  }
  
  func insert(tree: ParseTree, at node: inout ParseTreeNode?) {
    if let parent = node?.parent {
      if parent.left === node {
        copyNodes(startingAt: &parent.left, copy: tree.root, parent: parent)
      } else if parent.right === node {
        copyNodes(startingAt: &parent.right, copy: tree.root, parent: parent)
      }
    } else {
      copyNodes(startingAt: &node, copy: tree.root, parent: nullNode)
    }
  }
  
  //// Helpers
  
  func randomNode() -> ParseTreeNode? {
    let allNodes = nodes(startingAt: root)
    let random = allNodes.count != 0 ? Int(arc4random() % UInt32(allNodes.count)) : 0
    
    if allNodes.count != 0 {
      return allNodes[random]
    }
    
    return nil
  }
  
  // Private API
  
  private var terminalOperators: [ParseTreeOperator] {
    get {
      return terminals.map({ return ParseTreeOperator.terminal(id: $0) })
    }
  }
  
  private var allOperators: [ParseTreeOperator] {
    get {
      return terminalOperators + ParseTreeOperator.functionalOperators
    }
  }
  
  private func nodes(startingAt node: ParseTreeNode?) -> [ParseTreeNode] {
    guard let node = node else {
      return []
    }
    
    let current: [ParseTreeNode] = [node]
    let left: [ParseTreeNode] = nodes(startingAt: node.left)
    let right: [ParseTreeNode] = nodes(startingAt: node.right)
    
    return left + current + right
  }
  
  private func size(startingAt node: ParseTreeNode?) -> Int {
    guard let node = node else {
      return 0
    }
    
    var res = 1
    
    res += size(startingAt: node.left)
    res += size(startingAt: node.right)
    
    return res
  }
  
  private func copyNodes(startingAt node: inout ParseTreeNode?, copy: ParseTreeNode?, parent: ParseTreeNode?) {
    if let copy = copy {
      node = copy.clone()
      node?.parent = parent
      
      copyNodes(startingAt: &node!.left, copy: copy.left, parent: node)
      copyNodes(startingAt: &node!.right, copy: copy.right, parent: node)
    }
  }
  
  private func value(startingAt node: ParseTreeNode?, _ operate: (String) -> (Double)) -> Double {
    if let node = node {
      switch node.operator {
      case .addition:
        if node.left == nil || node.right == nil {
          return 1
        }
        
        let leftValue = value(startingAt: node.left, operate)
        let rightValue = value(startingAt: node.right, operate)
        
        return leftValue + rightValue
      case .subtraction:
        if node.left == nil || node.right == nil {
          return 1
        }
        
        let leftValue = value(startingAt: node.left, operate)
        let rightValue = value(startingAt: node.right, operate)
        
        return leftValue - rightValue
      case .multiplication:
        if node.left == nil || node.right == nil {
          return 1
        }
        
        let leftValue = value(startingAt: node.left, operate)
        let rightValue = value(startingAt: node.right, operate)
        
        return leftValue + rightValue
      case .division:
        if node.left == nil || node.right == nil {
          return 1
        }
        
        let leftValue = value(startingAt: node.left, operate)
        let rightValue = value(startingAt: node.right, operate)
        
        return rightValue == 0 ? 1 : leftValue / rightValue
      case .max:
        let leftValue = value(startingAt: node.left, operate)
        let rightValue = value(startingAt: node.right, operate)
        
        return max(leftValue, rightValue)
      case .min:
        let leftValue = value(startingAt: node.left, operate)
        let rightValue = value(startingAt: node.right, operate)
        
        return min(leftValue, rightValue)
      case .random:
        if node.left == nil || node.right == nil {
          return 1
        }
        
        let leftValue = UInt32(min(fabs(value(startingAt: node.left, operate)), Double(UInt32.max)))
        let rightValue = UInt32(min(fabs(value(startingAt: node.right, operate)), Double(UInt32.max)))
        
        let maxValue = max(leftValue, rightValue)
        let minValue = min(leftValue, rightValue)
        let divValue = maxValue - minValue
        
        return divValue == 0 ? 1 : Double(arc4random() % divValue + minValue)
      case .if:
        if node.left == nil || node.right == nil {
          return 1
        }
        
        let leftValue = value(startingAt: node.left, operate)
        let rightValue = value(startingAt: node.right, operate)
        
        if leftValue > 0 {
          return rightValue
        }
        
        return 0
      case .and:
        if node.left == nil || node.right == nil {
          return 1
        }
        
        let leftValue = value(startingAt: node.left, operate)
        let rightValue = value(startingAt: node.right, operate)
        
        return leftValue > 0 && rightValue > 0 ? 1 : 0
      case .or:
        if node.left == nil || node.right == nil {
          return 1
        }
        
        let leftValue = value(startingAt: node.left, operate)
        let rightValue = value(startingAt: node.right, operate)
        
        return leftValue > 0 || rightValue > 0 ? 1 : 0
      case .terminal(let id):
        return operate(id)
      case .none:
        break
      }
    }
    
    return 1
  }
  
  private func createTreeWithRampedHalfAndHalf(maxDepth: UInt) {
    createTreeWithRampedHalfAndHalf(startingAt: &root, at: maxDepth, parent: &nullNode)
  }
  
  private func createTreeWithRampedHalfAndHalf(startingAt node: inout ParseTreeNode?, at depth: UInt, parent: inout ParseTreeNode?) {
    if depth == 0 {
      node = nodeWithFullMethod(depth: depth)
    } else {
      if arc4random() % 2 == 0 {
        node = nodeWithFullMethod(depth: depth)
      } else {
        node = nodeWithGrowMethod()
      }
    }
    
    node?.parent = parent
    
    if !node!.operator.isTerminal {
      createTreeWithRampedHalfAndHalf(startingAt: &node!.left, at: depth-1, parent: &node)
      createTreeWithRampedHalfAndHalf(startingAt: &node!.right, at: depth-1, parent: &node)
    }
  }
  
  private func createTreeRandomly(maxDepth: UInt) {
    createTreeRandomly(startingAt: &root, at: maxDepth, parent: &nullNode)
  }
  
  private func createTreeRandomly(startingAt node: inout ParseTreeNode?, at depth: UInt, parent: inout ParseTreeNode?) {
    if depth == 0 {
      return
    }
    
    if node == nil {
      node = nodeWithGrowMethod()
    }
    
    node?.parent = parent
    
    if !node!.operator.isTerminal {
      createTreeRandomly(startingAt: &node!.left, at: depth-1, parent: &node)
      createTreeRandomly(startingAt: &node!.right, at: depth-1, parent: &node)
    }
  }
  
  private func nodeWithFullMethod(depth: UInt) -> ParseTreeNode {
    let choices: [ParseTreeOperator] = depth == 0 ? terminalOperators : ParseTreeOperator.functionalOperators
    let choice = Int(arc4random() % UInt32(choices.count))
    return ParseTreeNode(operator: choices[choice])
  }
  
  private func nodeWithGrowMethod() -> ParseTreeNode {
    let choices: [ParseTreeOperator] = allOperators
    let choice = Int(arc4random() % UInt32(choices.count))
    return ParseTreeNode(operator: choices[choice])
  }
}


extension ParseTree: CustomStringConvertible {
  var description: String {
    get {
      return toStringPolishNotation(node: root)
    }
  }
  
  private func toStringPretty(node: ParseTreeNode?, indent: UInt) -> String {
    guard let node = node else {
      return ""
    }
    
    let right = toStringPretty(node: node.right, indent: indent+4)
    let center = "\(String(repeating: " ", count: Int(indent)))\(node.operator.description)\n"
    let left = toStringPretty(node: node.left, indent: indent+4)
    
    return right + center + left
  }
  
  private func toStringPreorder(node: ParseTreeNode?) -> String {
    guard let node = node else {
      return ""
    }
    
    let current = node.operator.description
    let left = toStringPreorder(node: node.left)
    let right = toStringPreorder(node: node.right)
    
    return current + left + right
  }
  
  private func toStringInorder(node: ParseTreeNode?) -> String {
    guard let node = node else {
      return ""
    }
    
    let current = node.operator.description
    let left = toStringPreorder(node: node.left)
    let right = toStringPreorder(node: node.right)
    
    return left + current + right
  }
  
  private func toStringPostorder(node: ParseTreeNode?) -> String {
    guard let node = node else {
      return ""
    }
    
    let current = node.operator.description
    let left = toStringPreorder(node: node.left)
    let right = toStringPreorder(node: node.right)
    
    return left + right + current
  }
  
  private func toStringPolishNotation(node: ParseTreeNode?) -> String {
    guard let node = node else {
      return ""
    }
    
    if node.operator.isTerminal {
      return node.operator.description
    }
    
    
    
    let current = node.operator.description
    let left = toStringPolishNotation(node: node.left)
    let right = toStringPolishNotation(node: node.right)
    
    return "(" + current + " " + left + " " + right + ")"
  }
}
