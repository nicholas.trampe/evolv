//
//  Simulation.swift
//  evolv iOS
//
//  Created by Nicholas Trampe on 7/9/18.
//  Copyright © 2018 Nicholas Trampe. All rights reserved.
//

#if os(iOS)
import UIKit
#else
import Foundation
#endif

class Simulation: SimulationModel {
  weak var observer: SimulationModelObserver? = nil
  
  var size: Size { get {return config.game.size} }
  private(set) var updateInterval: Double = (1.0 / 10.0)
  var isRunning: Bool {
    get {
      return timer != nil
    }
  }
  var isTracking: Bool {
    get {
      return tracking != nil
    }
  }
  
  private let config: SimulationConfig
  private var populations: [PopulationID:Population] = [:]
  private var game: Game!
  private var tracking: Organism? = nil
  private var updateQueue: OperationQueue = OperationQueue()
  private var timer: Timer? = nil
  private var metrics: SimulationMetrics = SimulationMetrics()
  
  init(config: SimulationConfig) {
    self.config = config
    updateQueue.qualityOfService = .background
    updateQueue.maxConcurrentOperationCount = 1
    
    for (populationID, populationConfig) in config.populations {
      populations[populationID] = Population(id: populationID, config: populationConfig)
    }
  }
  
  func setUpdateInterval(_ interval: Double) {
    updateInterval = interval
    
    DispatchQueue.main.async {
      self.observer?.didUpdate(interval: self.updateInterval)
    }
  }
  
  func start() {
    restart()
    
    observer?.didUpdate(metrics: metrics)
    
    startTimer()
  }
  
  func pause() {
    stopTimer()
  }
  
  func resume() {
    startTimer()
  }
  
  func fastForward() {
    if updateInterval > (1.0 / 32.0) {
      setUpdateInterval(updateInterval / 2.0)
    } else {
      setUpdateInterval(1.0 / 1.0)
    }
    
    startTimer()
  }
  
  func organism(at location: Point) -> Organism? {
    return game.state.closestOrganism(at: location)
  }
  
  func startTracking(organism: Organism) {
    stopTracking()
    tracking = organism
    
    DispatchQueue.main.async {
      self.observer?.didStartTrackingOrganism(organism: organism)
    }
  }
  
  func stopTracking() {
    if let tracking = tracking {
      DispatchQueue.main.async {
        self.observer?.didStopTrackingOrganism(organism: tracking)
      }
    }
    
    tracking = nil
  }
  
  private func restart() {
    updateQueue.cancelAllOperations()
    
    if let tracking = tracking {
      DispatchQueue.main.async {
        self.observer?.didStopTrackingOrganism(organism: tracking)
      }
      self.tracking = nil
    }
    
    game = Game(config: config.game, observer: self)
    for (id, population) in populations {
      game.controllers[id] = population
    }
    
    var locations = game.state.emptyPositions.shuffled()
    
    for (_, population) in populations {
      for (id, _) in population.individuals {
        let location = locations.removeFirst()
        var org = Organism(id: id,
                           populationID: population.id,
                           preyMask: population.config.attackMask,
                           predatorMask: config.predators(for: population.id),
                           canEat: population.config.canEatFood,
                           energy: config.game.startingOrganismEnergy)
        org.location = location
        game.addOrganism(org)
      }
    }
  }
  
  private func update() {
    game.update()
    observer?.didUpdate(time: game.time)
    
    metrics.fitnessEvaluations += populations.map({$1}).reduce(0, { $0 + UInt64($1.config.populationSize + $1.config.offspringSize) })
    
    if game.isOver {
      for (id, population) in populations {
        metrics.add(id: id, metrics: population.metrics)
      }
      
      DispatchQueue.main.sync {
        observer?.didUpdate(metrics: metrics)
      }
      
      if (config.terminationOptions.contains(.maximumNumberOfEvaluations) && metrics.fitnessEvaluations >= config.maximumNumberOfEvaluations) ||
         (config.terminationOptions.contains(.bestFitnessConverged) && metrics.minimunPercentChange <= 0.05) {
        pause()
        observer?.didFinishSimulation()
      } else {
        for (_, population) in populations {
          population.createGeneration()
        }
        restart()
      }
    }
  }
  
  private func stopTimer() {
    timer?.invalidate()
    timer = nil
    
    updateQueue.cancelAllOperations()
  }
  
  private func startTimer() {
    stopTimer()
    
    timer = Timer.scheduledTimer(withTimeInterval: updateInterval, repeats: true, block: { t in
      self.updateQueue.addOperation {
        self.update()
      }
    })
  }
}

extension Simulation: GameObserver {
  func didAdd(organism: Organism) {
    DispatchQueue.main.async {
      self.observer?.didAdd(organism: organism)
    }
  }
  
  func didPerform(action: Action, result: ActionResult) {
    DispatchQueue.main.async {
      self.observer?.didPerform(action: action, result: result)
      
      self.checkTracking(organism: result.actingOrganism)
    }
  }
  
  func didRemove(organism: Organism) {
    DispatchQueue.main.async {
      self.observer?.didRemove(organism: organism)
      
      if let tracking = self.tracking, tracking == organism {
        self.observer?.didStopTrackingOrganism(organism: organism)
      }
    }
  }
  
  func didAdd(item: Item) {
    DispatchQueue.main.async {
      self.observer?.didAdd(item: item)
    }
  }
  
  func checkTracking(organism: Organism) {
    if let tracking = self.tracking {
      if organism == tracking {
        self.observer?.didTrack(organism: organism)
      }
    }
  }
}
