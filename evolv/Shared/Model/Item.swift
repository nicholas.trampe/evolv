//
//  Item.swift
//  evolv
//
//  Created by Nicholas Trampe on 7/26/18.
//  Copyright © 2018 Nicholas Trampe. All rights reserved.
//

import Foundation

enum ItemType {
  case food
  case water
  case wall
  
  var isPassable: Bool {
    get {
      switch self {
      case .food:
        return true
      case .water:
        return false
      case .wall:
        return false
      }
    }
  }
}

struct Item {  
  let id: UInt32
  let type: ItemType
  var location: Point = .zero
}
