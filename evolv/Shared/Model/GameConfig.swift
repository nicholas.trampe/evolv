//
//  GameConfig.swift
//  evolv
//
//  Created by Nicholas Trampe on 7/20/18.
//  Copyright © 2018 Nicholas Trampe. All rights reserved.
//

import Foundation

struct GameConfig {
  let size: Size
  
  let moveEnergyLoss: UInt
  let moveThirstGain: UInt
  let attackEnergyGainCoefficient: Double
  let attackThirstGain: UInt
  let eatThirstGain: UInt
  let restEnergyLoss: UInt
  let restThirstGain: UInt
  let drinkThirstLoss: UInt
  let drinkEnergyLoss: UInt
  
  let timeLimit: UInt
  let minimumNumberOfSurvivingOrganisms: UInt
  let foodEnergy: UInt
  let foodDensity: Double
  let startingOrganismEnergy: UInt
}
