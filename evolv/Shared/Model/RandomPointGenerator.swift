//
//  RandomPointGenerator.swift
//  evolv
//
//  Created by Nicholas Trampe on 7/26/18.
//  Copyright © 2018 Nicholas Trampe. All rights reserved.
//

import Foundation

struct RandomPointGenerator {
  static func randomPoints(availablePoints: [Point], density: Double) -> [Point] {
    var locations = Set<Point>(availablePoints)
    var res: [Point] = []
    
    while locations.count > 0 {
      if let location = locations.popFirst() {
        if (Double(arc4random() % 100) / Double(100)) < density {
          res.append(location)
        }
      }
    }
    
    return res
  }
  
  static func randomPoints(availablePoints: [Point], count: Int) -> [Point] {
    precondition(availablePoints.count >= count, "Not enough available points")
    
    var locations = Set<Point>(availablePoints).shuffled()
    var res: [Point] = []
    
    while res.count < count && locations.count > 0 {
      res.append(locations.removeFirst())
    }
    
    return res
  }
  
  static func dispersedPoints(availablePoints: [Point], maximumNumberOfPoints: Int, minimumDistance: Double) -> [Point] {
    var centerPoints: [Point] = []
    var locations = availablePoints
    
    while centerPoints.count < maximumNumberOfPoints && locations.count > 0 {
      let location = locations.removeFirst()
      let distance = centerPoints.map({$0.distance(from: location)}).min() ?? Double.greatestFiniteMagnitude
      if distance > minimumDistance {
        centerPoints.append(location)
      }
    }
    
    return centerPoints
  }
  
  static func randomShape(around centerPoint: Point, containedIn rect: Rect, depth: Int) -> [Point] {
    var res: [Point] = []
    
    if depth == 0 {
      return res
    }
    
    for nearbyPoint in centerPoint.nearby {
      if !rect.contains(point: nearbyPoint) {
        continue
      }
      
      if arc4random() % 2 == 0 || depth == 1 {
        res.append(nearbyPoint)
        res += RandomPointGenerator.randomShape(around: nearbyPoint, containedIn: rect, depth: depth-1)
      }
    }
    
    return res
  }
  
  static func randomGroupings(availablePoints: [Point], containedIn rect: Rect, maximumNumberOfGroups: Int, minimumDistance: Double, depthOfGroup: Int) -> [Point] {
    var points = Set<Point>()
    let centerPoints = dispersedPoints(availablePoints: availablePoints, maximumNumberOfPoints: maximumNumberOfGroups, minimumDistance: minimumDistance)
    
    for location in centerPoints {
      let shape = randomShape(around: location, containedIn: rect, depth: depthOfGroup)
      points = points.union(shape)
    }
    
    return Array(points)
  }
}
