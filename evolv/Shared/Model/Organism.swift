//
//  Organism.swift
//  evolv iOS
//
//  Created by Nicholas Trampe on 7/16/18.
//  Copyright © 2018 Nicholas Trampe. All rights reserved.
//

import Foundation

struct Organism {
  let id: UInt32
  let populationID: PopulationID
  let preyMask: PopulationIDMask
  let predatorMask: PopulationIDMask
  let canEat: Bool
  var location: Point = .zero
  private(set) var energy: UInt = 0
  private(set) var thirst: UInt = 0
  
  init(id: UInt32, populationID: PopulationID, preyMask: PopulationIDMask, predatorMask: PopulationIDMask, canEat: Bool, energy: UInt) {
    self.id = id
    self.populationID = populationID
    self.preyMask = preyMask
    self.predatorMask = predatorMask
    self.canEat = canEat
    self.energy = energy
  }
  
  func isPrey(potentialPrey: Organism) -> Bool {
    return preyMask.rawValue & potentialPrey.populationID.rawValue != 0
  }
  
  func isPredator(potentialPredator: Organism) -> Bool {
    return predatorMask.rawValue & potentialPredator.populationID.rawValue != 0
  }
  
  func hasFriends() -> Bool {
    var allMask = PopulationIDMask.all
    allMask.remove(PopulationIDMask(rawValue: populationID.rawValue))
    return preyMask != allMask
  }
  
  func hasPredators() -> Bool {
    return predatorMask != .none
  }
  
  func hasPrey() -> Bool {
    return preyMask != .none
  }
  
  mutating func loseEnergy(amount: UInt) {
    if amount <= energy {
      energy -= amount
    } else {
      energy = 0
    }
  }
  
  mutating func gainEnergy(amount: UInt) {
    energy += amount
  }
  
  mutating func loseThirst(amount: UInt) {
    if amount <= thirst {
      thirst -= amount
    } else {
      thirst = 0
    }
  }
  
  mutating func gainThirst(amount: UInt) {
    thirst += amount
  }
}

extension Organism: CustomStringConvertible {
  public var description: String {
    get {
      var res = ""
      res += "ID: \(id)\n"
      res += "Population ID: \(populationID)\n"
      res += "Location: \(location)\n"
      res += "Energy: \(energy)\n"
      res += "Thirst: \(thirst)\n"
      
      return res
    }
  }
}

extension Organism: Equatable {
  static func == (lhs: Organism, rhs: Organism) -> Bool {
    return lhs.id == rhs.id && lhs.populationID == rhs.populationID
  }
}
