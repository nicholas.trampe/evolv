//
//  QuadTree.swift
//  evolv
//
//  Created by Nicholas Trampe on 7/25/18.
//  Copyright © 2018 Nicholas Trampe. All rights reserved.
//

import Foundation

protocol PointsContainer {
  var count: Int { get }
  
  func add(point: Point) -> Bool
  func remove(point: Point) -> Bool
  func points(in rect: Rect) -> [Point]
}

class QuadTree: PointsContainer {
  private let root: QuadTreeNode
  
  init(rect: Rect) {
    self.root = QuadTreeNode(rect: rect)
  }
  
  var count: Int {
    get {
      return root.count
    }
  }
  
  @discardableResult
  func add(point: Point) -> Bool {
    return root.add(point: point)
  }
  
  @discardableResult
  func remove(point: Point) -> Bool {
    return root.remove(point: point)
  }
  
  func points(in rect: Rect) -> [Point] {
    return root.points(in: rect)
  }
}

class QuadTreeNode: PointsContainer {
  enum NodeType {
    case leaf
    case `internal`(children: [QuadTreeNode])
  }
  
  var points: [Point] = []
  let rect: Rect
  var type: NodeType = .leaf
  
  static let maxPointCapacity = 3
  
  init(rect: Rect) {
    self.rect = rect
  }
  
  var count: Int {
    get {
      switch type {
      case .leaf:
        return points.count
      case .internal(let children):
        var n: Int = points.count
        for child in children {
          n += child.count
        }
        return n
      }
    }
  }
  
  @discardableResult
  func add(point: Point) -> Bool {
    if !rect.contains(point: point) {
      return false
    }
    
    switch type {
    case .internal(let children):
      for child in children {
        if child.add(point: point) {
          return true
        }
      }
      
      fatalError("rect.contains evaluated to true, but none of the children added the point")
      
    case .leaf:
      points.append(point)
      
      if points.count == QuadTreeNode.maxPointCapacity {
        subdivide()
      }
    }
    
    return true
  }
  
  @discardableResult
  func remove(point: Point) -> Bool {
    if !rect.contains(point: point) {
      return false
    }
    
    if let index = points.firstIndex(of: point) {
      points.remove(at: index)
      return true
    }
    
    switch type {
    case .internal(let children):
      
      for child in children {
        if child.remove(point: point) {
          let pointsInChildrenCount = children.reduce(0, { $0 + $1.points.count })
          
          if pointsInChildrenCount == 0 {
            type = .leaf
          }
          
          return true
        }
      }
      
      fatalError("rect.contains evaluated to true, but none of the children removed the point")
    case .leaf:
      break
    }
    
    return false
  }
  
  func points(in rect: Rect) -> [Point] {
    if !self.rect.intersects(rect: rect) {
      return []
    }
    
    var res: [Point] = []
    
    for point in points {
      if rect.contains(point: point) {
        res.append(point)
      }
    }
    
    switch type {
    case .internal(let children):
      for child in children {
        res += child.points(in: rect)
      }
    case .leaf:
      break
    }
    
    return res
  }
  
  private func subdivide() {
    switch type {
    case .internal(_):
      preconditionFailure("Cannot subdivide on an internal node")
    case .leaf:
      let leftTop = QuadTreeNode(rect: rect.leftTopRect)
      let leftBottom = QuadTreeNode(rect: rect.leftBottomRect)
      let rightTop = QuadTreeNode(rect: rect.rightTopRect)
      let rightBottom = QuadTreeNode(rect: rect.rightBottomRect)
      let nodes = [leftTop,leftBottom,rightTop,rightBottom]
      type = .internal(children: nodes)
    }
  }
  
  var recursiveDescription: String {
    return recursiveDescription(withTabCount: 0)
  }
  
  private func recursiveDescription(withTabCount count: Int) -> String {
    let indent = String(repeating: "\t", count: count)
    var result = "\(indent)" + description + "\n"
    switch type {
    case .internal(let children):
      for child in children {
        result += child.recursiveDescription(withTabCount: count + 1)
      }
    default:
      break
    }
    return result
  }
}

extension QuadTreeNode: CustomStringConvertible {
  var description: String {
    switch type {
    case .leaf:
      return "leaf \(rect) Points: \(points)"
    case .internal:
      return "parent \(rect) Points: \(points)"
    }
  }
}

extension QuadTree: CustomStringConvertible {
  public var description: String {
    return "Quad tree\n" + root.recursiveDescription
  }
}
