//
//  EnvironmentState.swift
//  evolv
//
//  Created by Nicholas Trampe on 7/20/18.
//  Copyright © 2018 Nicholas Trampe. All rights reserved.
//

import Foundation

enum Action {
  case move(fromPoint: Point, toPoint: Point)
  case attack(predatorPoint: Point, preyPoint: Point)
  case eat(organismPoint: Point, foodPoint: Point)
  case drink(organismPoint: Point, waterPoint: Point)
  case rest(organismPoint: Point)
  
  var direction: Direction {
    get {
      switch self {
      case .move(let fromPoint, let toPoint):
        return fromPoint.direction(to: toPoint)
      case .attack(let predatorPoint, let preyPoint):
        return predatorPoint.direction(to: preyPoint)
      case .eat(let organismPoint, let foodPoint):
        return organismPoint.direction(to: foodPoint)
      case .drink(let organismPoint, let waterPoint):
        return organismPoint.direction(to: waterPoint)
      case .rest(_):
        return .none
      }
    }
  }
}

struct ActionResult {
  let affectedOrganisms: [Organism]
  let affectedItems: [Item]
  
  var actingOrganism: Organism {
    get {
      guard affectedOrganisms.count > 0 else {
        fatalError("Action result should have at least one affected organism")
      }
      
      return affectedOrganisms.first!
    }
  }
}

struct GameState {
  let config: GameConfig
  
  private var organisms: [Point:Organism] = [:]
  private var items: [Point:Item] = [:]
  
  init(config: GameConfig) {
    self.config = config
  }
  
  // Public API
  
  mutating func add(organism: Organism) {
    precondition(organisms[organism.location] == nil, "Attempting to add an organism where one already exists")
    
    organisms[organism.location] = organism
  }
  
  mutating func remove(organism: Organism) {
    precondition(organisms[organism.location] != nil, "Attempting to remove an organism that doesnt exist")
    
    organisms[organism.location] = nil
  }
  
  mutating func add(item: Item) {
    precondition(items[item.location] == nil, "Attempting to add item where one already exists")
    
    items[item.location] = item
  }
  
  mutating func remove(item: Item) {
    precondition(items[item.location] != nil, "Attempting to remove item that doesnt exist")
    
    items[item.location] = nil
  }
  
  func organism(at point: Point) -> Organism? {
    return organisms[point]
  }
  
  func item(at point: Point) -> Item? {
    return items[point]
  }
  
  func items(of type: ItemType) -> [Point:Item] {
    return items.filter({ $1.type == type })
  }
  
  func iterateItems(_ check: (Item) -> ()) {
    items.forEach { (loc, item) in
      check(item)
    }
  }
  
  func iterateOrganisms(_ check: (Organism) -> ()) {
    organisms.forEach { (loc, org) in
      check(org)
    }
  }
  
  var allOrganisms: [Organism] {
    get {
      return organisms.map({$1})
    }
  }
  
  var organismPositions: Set<Point> {
    get {
      return Set(organisms.map({$1.location}))
    }
  }
  
  var numberOfItems: Int {
    get {
      return items.count
    }
  }
  
  var itemPositions: Set<Point> {
    get {
      return Set(items.map({$1.location}))
    }
  }
  
  var filledPositions: Set<Point> {
    get {
      return organismPositions.union(itemPositions)
    }
  }
  
  var allPositions: Set<Point> {
    get {
      let rect = Rect(size: config.size)
      return rect.allPoints
    }
  }
  
  var emptyPositions: Set<Point> {
    get {
      return allPositions.subtracting(filledPositions)
    }
  }
  
  @discardableResult
  mutating func perform(action: Action) -> ActionResult {
    switch action {
    case .move(let fromPoint, let toPoint):
      return move(fromPoint: fromPoint, toPoint: toPoint)
    case .attack(let predatorPoint, let preyPoint):
      return attack(predatorPoint: predatorPoint, preyPoint: preyPoint)
    case .eat(let organismPoint, let foodPoint):
      return eat(organismPoint: organismPoint, foodPoint: foodPoint)
    case .drink(let organismPoint, let waterPoint):
      return drink(organismPoint: organismPoint, waterPoint: waterPoint)
    case .rest(let organismPoint):
      return restOrganism(at: organismPoint)
    }
  }
  
  func randomActionForOrganism(at location: Point) -> Action? {
    let actions = possibleActionsForOrganism(at: location)
    let random = Int(arc4random() % UInt32(actions.count))
    
    return actions[random]
  }
  
  func possibleActionsForOrganism(at point: Point) -> [Action] {
    guard let organism = self.organisms[point] else {
      preconditionFailure("Organism not found at point")
    }
    
    var actions = [Action.rest(organismPoint: point)]
    
    let enemyPoints = searchNearbyOrganisms(at: point, depth: 1, check: { organism.isPrey(potentialPrey: $0) })
    let attackActions = enemyPoints.map { return Action.attack(predatorPoint: point, preyPoint: $0) }
    
    actions += attackActions
    
    let emptySpaces = searchNearbyEmptyPoints(around: point, depth: 1)
    let moveActions = emptySpaces.map { return Action.move(fromPoint: point, toPoint: $0) }
    
    actions += moveActions
    
    if organism.canEat {
      let foodPoints = searchNearbyItem(at: point, depth: 1, check: { $0.type == .food })
      let eatActions = foodPoints.map { return Action.eat(organismPoint: point, foodPoint: $0) }
      
      actions += eatActions
    }
    
    let waterPoints = searchNearbyItem(at: point, depth: 1, check: { $0.type == .water })
    let drinkActions = waterPoints.map { return Action.drink(organismPoint: point, waterPoint: $0) }
    
    actions += drinkActions
    
    return actions
  }
  
  @discardableResult
  func searchNearbyPoints(around point: Point, depth: UInt, check: (Point) -> Bool) -> [Point] {
    return point.nearby(check: {
      return check($0)
    }, depth: depth)
  }
  
  @discardableResult
  func searchNearbyOrganisms(at point: Point, depth: UInt, check: (Organism) -> Bool) -> [Point] {
    if organisms.count == 0 {
      return []
    }
    
    return searchNearbyPoints(around: point, depth: depth) { nearbyPoint in
      if let nearbyOrganism = self.organism(at: nearbyPoint) {
        return check(nearbyOrganism)
      }
      
      return false
    }
  }
  
  func searchNearbyItem(at point: Point, depth: UInt, check: (Item) -> Bool) -> [Point] {    
    if items.count == 0 {
      return []
    }
    
    return searchNearbyPoints(around: point, depth: depth) { nearbyPoint in
      if self.organism(at: nearbyPoint) != nil {
        return false
      }
      
      if let item = self.item(at: nearbyPoint) {
        return check(item)
      } 
      
      return false
    }
  }
  
  func searchNearbyEmptyPoints(around point: Point, depth: UInt) -> [Point] {
    let bounds = Rect(0,0,config.size.width-1,config.size.height-1)
    
    return searchNearbyPoints(around: point, depth: depth, check: { nearbyPoint -> Bool in
      if !bounds.contains(point: nearbyPoint) {
        return false
      }
      
      if self.organism(at: nearbyPoint) != nil {
        return false
      }
      
      if let item = item(at: nearbyPoint) {
        if !item.type.isPassable {
          return false
        }
      }
      
      return true
    })
  }
  
  func closestOrganism(at point: Point) -> Organism? {
    
    if let org = organism(at: point) {
      return org
    }
    
    if let near = point.nearest(check: { self.organism(at: $0) != nil }, depth: 2) {
      return organism(at: near)
    }
    
    return nil
  }
  
  // Private API
  
  @discardableResult
  private mutating func move(fromPoint: Point, toPoint: Point) -> ActionResult {
    guard var organism = organisms[fromPoint] else {
      preconditionFailure("Organism not found at point")
    }
    
    organism.location = toPoint
    organism.loseEnergy(amount: config.moveEnergyLoss)
    organism.gainThirst(amount: config.moveThirstGain)
    
    organisms[toPoint] = organism
    organisms[fromPoint] = nil
    
    return ActionResult(affectedOrganisms: [organism], affectedItems: [])
  }
  
  @discardableResult
  private mutating func eat(organismPoint: Point, foodPoint: Point) -> ActionResult {
    guard var organism = organisms[organismPoint], let food = items[foodPoint], food.type == .food else {
      preconditionFailure("Food or organism not found at point")
    }
    
    organism.gainEnergy(amount: config.foodEnergy)
    organism.gainThirst(amount: config.eatThirstGain)
    items[foodPoint] = nil
    
    
    organism.location = foodPoint
    organisms[foodPoint] = organism
    organisms[organismPoint] = nil
    
    return ActionResult(affectedOrganisms: [organism], affectedItems: [food])
  }
  
  @discardableResult
  private mutating func drink(organismPoint: Point, waterPoint: Point) -> ActionResult {
    guard var organism = organisms[organismPoint], let water = items[waterPoint], water.type == .water else {
      preconditionFailure("Water or organism not found at point")
    }
    
    organism.loseThirst(amount: config.drinkThirstLoss)
    organism.loseEnergy(amount: config.drinkEnergyLoss)
    
    organisms[organismPoint] = organism
    
    return ActionResult(affectedOrganisms: [organism], affectedItems: [water])
  }
  
  @discardableResult
  private mutating func attack(predatorPoint: Point, preyPoint: Point) -> ActionResult {
    guard var predator = organisms[predatorPoint], var prey = organisms[preyPoint] else {
      preconditionFailure("Organism not found at point")
    }
    
    let energyTransfer = UInt(Double(prey.energy) * config.attackEnergyGainCoefficient)
    predator.gainEnergy(amount: energyTransfer)
    predator.gainThirst(amount: config.attackThirstGain)
    prey.loseEnergy(amount: energyTransfer)
    
    organisms[predatorPoint] = predator
    organisms[preyPoint] = prey
    
    return ActionResult(affectedOrganisms: [predator, prey], affectedItems: [])
  }
  
  @discardableResult
  private mutating func restOrganism(at point: Point) -> ActionResult {
    guard var organism = organisms[point] else {
      preconditionFailure("Organism not found at point")
    }
    
    organism.loseEnergy(amount: config.restEnergyLoss)
    organism.gainThirst(amount: config.restThirstGain)
    
    organisms[point] = organism
    
    return ActionResult(affectedOrganisms: [organism], affectedItems: [])
  }
}

extension GameState: CustomStringConvertible {
  var description: String {
    get {
      let size = config.size
      var str = String(repeating: "-", count: Int(size.width)) + "\n"
      
      for y in 0..<Int(size.height) {
        for x in 0..<Int(size.width) {
          let point = Point(Double(x),size.height - Double(y) - 1)
          
          if organism(at: point) != nil {
            str.append(" o")
          } else if let item = item(at: point) {
            switch item.type {
            case .food: str.append(" f")
            case .water: str.append(" w")
            case .wall: str.append(" D")
            }
          } else {
            str.append("  ")
          }
        }
        
        str.append("\n")
      }
      
      str += String(repeating: "-", count: Int(size.width))
      
      return str
    }
  }
}
