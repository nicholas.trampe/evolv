//
//  SimulationMetrics.swift
//  evolv
//
//  Created by Nicholas Trampe on 7/21/18.
//  Copyright © 2018 Nicholas Trampe. All rights reserved.
//

import Foundation

struct PopulationMetrics {
  let generation: UInt
  let bestFitness: Double
  let averageFitness: Double
  let worstFitness: Double
  let bestIndividual: Individual?
}


extension PopulationMetrics: CustomStringConvertible {
  var description: String {
    get {
      return fullDescription
    }
  }
  
  var conciseDescription: String {
    get {
      var res = ""
      res += "Generations: \(generation)\n"
      res += "Average Fitness: \(String(format: "%.2f", averageFitness))\n"
      res += "Best Fitness: \(String(format: "%.2f", bestFitness))\n"
      res += "Worst Fitness: \(String(format: "%.2f", worstFitness))\n"
      return res
    }
  }
  
  var fullDescription: String {
    get {
      var res = conciseDescription
      
      res += "Individual:\n\(bestIndividual?.tree.description ?? "None"))"
      
      return res
    }
  }
}

struct SimulationMetrics {
  private(set) var populationMetrics: [PopulationID:[PopulationMetrics]] = [:]
  var fitnessEvaluations: UInt64 = 0
  
  mutating func add(id: PopulationID, metrics: PopulationMetrics) {
    if populationMetrics[id] == nil {
      populationMetrics[id] = []
    }
    
    populationMetrics[id]?.append(metrics)
  }
  
  func bestOverallFitness(for id: PopulationID) -> Double {
    return populationMetrics[id]?.map({$0.bestFitness}).max() ?? 0
  }
  
  func worstOverallFitness(for id: PopulationID) -> Double {
    return populationMetrics[id]?.map({$0.bestFitness}).min() ?? 0
  }
  
  func averageChangeOverTime(for id: PopulationID) -> Double {
    guard let metrics = populationMetrics[id],
          metrics.count > 2,
          let bests = populationMetrics[id]?.map({$0.bestFitness}) else {
      return 1
    }
    
    var derivative: [Double] = []
    
    for (index, fitness) in bests.enumerated() {
      if index > 0 {
        let previous = bests[index-1]
        derivative.append(fitness - previous)
      } else {
        derivative.append(0)
      }
    }
    
    let averageChange = derivative.reduce(0, +) / Double(derivative.count)
    
    return averageChange
  }
  
  func percentChangeOverTime(for id: PopulationID) -> Double {
    guard let metrics = populationMetrics[id],
      metrics.count > 2 else {
        return 1
    }
    
    let averageChange = averageChangeOverTime(for: id)
    let maxFitness = bestOverallFitness(for: id)
    let percentChange = maxFitness > 0 ? (averageChange / maxFitness) : 1
    
    return fabs(percentChange)
  }
  
  var minimunPercentChange: Double {
    get {
      if let res: Double = populationMetrics.map({ self.percentChangeOverTime(for: $0.key) }).min() {
        return res
      }
      
      return 1
    }
  }
}

extension SimulationMetrics: CustomStringConvertible {
  var description: String {
    get {
      var res = "Simulation Metrics:\n"
      
      res += "Number of Evaluations: \(fitnessEvaluations)\n\n"
      
      if populationMetrics.count == 0 {
        return res
      }
      
      res += String(repeating: "-", count: 40) + "\n\n"
      
      for (id, metrics) in populationMetrics {
        
        res += "Population: \(id)\n"
        res += "Best Overall Fitness: \(String(format: "%.2f", bestOverallFitness(for: id)))\n"
        res += "Worst Overall Fitness: \(String(format: "%.2f", worstOverallFitness(for: id)))\n"
        res += "Average Change Over Time: \(String(format: "%.2f", averageChangeOverTime(for: id)))\n"
        res += "Percent Change Over Time: %\(String(format: "%.2f", percentChangeOverTime(for: id) * 100))\n"
        
        if let current = metrics.last {
          res += "\n\nCurrent Population:\n\(current.conciseDescription)\n\n"
        }
        
        res += String(repeating: "-", count: 40) + "\n\n"
      }
      
      return res
    }
  }
}
