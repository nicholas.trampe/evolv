//
//  Environment.swift
//  evolv iOS
//
//  Created by Nicholas Trampe on 7/16/18.
//  Copyright © 2018 Nicholas Trampe. All rights reserved.
//

import Foundation

protocol GameController: AnyObject {
  func nextAction(for organism: Organism, state: GameState) -> Action?
  func didUpdate(organism: Organism)
}

protocol GameObserver: AnyObject {
  func didAdd(organism: Organism)
  func didRemove(organism: Organism)
  func didAdd(item: Item)
  
  func didPerform(action: Action, result: ActionResult)
}

struct Game {
  weak var observer: GameObserver? = nil
  var controllers: [PopulationID:GameController] = [:]
  
  private let config: GameConfig
  private var itemID: UInt32 = 0
  
  private(set) var time: UInt = 0
  private(set) var state: GameState
  
  var isOver: Bool {
    get {
      if time >= config.timeLimit {
        return true
      }
      
      var numbers: [PopulationID:UInt] = [:]
      state.allOrganisms.forEach { (org) in
        if let current = numbers[org.populationID] {
          numbers[org.populationID] = current + 1
        } else {
          numbers[org.populationID] = 1
        }
      }
      
      for (_, popSize) in numbers {
        if popSize <= config.minimumNumberOfSurvivingOrganisms {
          return true
        }
      }
      
      return false
    }
  }
  
  init(config: GameConfig, observer: GameObserver?) {
    self.config = config
    self.observer = observer
    self.state = GameState(config: config)
    
    addWater()
    addWalls()
    addFoods()
  }
  
  // Public API
  
  mutating func update() {
    let oldOrganisms = self.state.allOrganisms
    oldOrganisms.shuffled().forEach { (organism) in
      
      // Check to see if the organism has already been killed by a previous organism
      guard state.organism(at: organism.location) != nil else {
        return
      }
      
      if let action = controllers[organism.populationID]?.nextAction(for: organism, state: state) {
        let result = state.perform(action: action)
        
        observer?.didPerform(action: action, result: result)
        
        for updatedOrganism in result.affectedOrganisms {
          if updatedOrganism.energy == 0 {
            state.remove(organism: updatedOrganism)
            observer?.didRemove(organism: updatedOrganism)
          }
          
          controllers[updatedOrganism.populationID]?.didUpdate(organism: updatedOrganism)
        }
      }
    }
    
    addFoodRandomly()
    time += 1
  }
  
  mutating func addOrganism(_ organism: Organism) {
    state.add(organism: organism)
    observer?.didAdd(organism: organism)
  }
  
  mutating private func addFoods() {
    let locations = RandomPointGenerator.randomPoints(availablePoints: state.emptyPositions.shuffled(), density: config.foodDensity)
    locations.forEach({ self.addItem(type: .food, at: $0) })
  }
  
  mutating private func addFoodRandomly() {
    var locations = state.emptyPositions.shuffled()
    
    while ((Double(state.items(of: .food).count) / (config.size.width * config.size.height)) < config.foodDensity && locations.count > 0) {
      let location = locations.removeLast()
      addItem(type: .food, at: location)
    }
  }
  
  mutating private func addWater() {
    let bounds = Rect(0,0,config.size.width-1,config.size.height-1)
    let points = RandomPointGenerator.randomGroupings(availablePoints: state.emptyPositions.shuffled(),
                                                      containedIn: bounds,
                                                      maximumNumberOfGroups: 20,
                                                      minimumDistance: 12,
                                                      depthOfGroup: 3)
    points.forEach({ self.addItem(type: .water, at: $0) })
  }
  
  mutating private func addWalls() {
    let points = RandomPointGenerator.dispersedPoints(availablePoints: state.emptyPositions.shuffled(), maximumNumberOfPoints: 10, minimumDistance: 20)
    points.forEach({ self.addItem(type: .wall, at: $0) })
  }
  
  mutating private func addItem(type: ItemType, at location: Point) {
    let item = Item(id: itemID, type: type, location: location)
    itemID += 1
    
    state.add(item: item)
    observer?.didAdd(item: item)
  }
}

extension Game: CustomStringConvertible {
  var description: String {
    get {
      return "\(state)\n\(time)"
    }
  }
}
