//
//  Types.swift
//  evolv iOS
//
//  Created by Nicholas Trampe on 7/11/18.
//  Copyright © 2018 Nicholas Trampe. All rights reserved.
//

#if os(OSX)
import Foundation
#elseif os(iOS)
import UIKit
#endif

enum Direction: UInt8 {
  case none = 0
  case north = 1
  case south = 2
  case east = 3
  case west = 4
  case northwest = 5
  case northeast = 6
  case southwest = 7
  case southeast = 8
  
  static let allDirections: [Direction] = [.none,.north,.south,.east,.west,.northwest,.northeast,.southwest,.southeast]
}

extension Direction {
  var vector: CGVector {
    get {
      var res = CGVector.zero
      switch self {
      case .north:
        res.dy += 1
      case .south:
        res.dy -= 1
      case .east:
        res.dx += 1
      case .west:
        res.dx -= 1
      case .northwest:
        res.dy += 1
        res.dx -= 1
      case .northeast:
        res.dy += 1
        res.dx += 1
      case .southwest:
        res.dy -= 1
        res.dx -= 1
      case .southeast:
        res.dy -= 1
        res.dx += 1
      case .none:
        break
      }
      
      return res
    }
  }
}

struct Size {
  var width: Double
  var height: Double
  
  public init(_ width: Double, _ height: Double) {
    self.width = width
    self.height = height
  }
  
  func randomPoint() -> Point {
    return .random(x: self.width, y: self.height)
  }
  
  func randomPoints(count: Int) -> Set<Point> {
    precondition(count > 0, "Count must be greater than zero")
    precondition(Double(count) < self.width * self.height, "Count must be less than the available choices")
    
    var locations = Set<Point>()
    while locations.count < count {
      locations.insert(self.randomPoint())
    }
    
    return locations
  }
  
  var half: Size {
    return Size(width / 2, height / 2)
  }
}

extension Size: CustomStringConvertible {
  public var description: String {
    return "(\(width), \(height))"
  }
}

struct Point: Hashable {
  var x: Double
  var y: Double
  
  public init(_ x: Double, _ y: Double) {
    self.x = x
    self.y = y
  }
  
  static var zero: Point = Point(0, 0)
  
  static func random(x: Double, y: Double) -> Point {
    return Point(Double(arc4random() % UInt32(x)), Double(arc4random() % UInt32(y)))
  }
  
  func distance(from point: Point) -> Double {
    let xDist = self.x - point.x
    let yDist = self.y - point.y
    return sqrt((xDist * xDist) + (yDist * yDist))
  }
  
  func distance(from points: [Point]) -> Double {
    guard points.count > 0 else { return Double.greatestFiniteMagnitude }
    
    let sum: Double = points.reduce(0.0, { (result, point) -> Double in
      return result + distance(from: point)
    })
    
    return sum / Double(points.count)
  }
  
  func move(direction: Direction) -> Point {
    var res = self
    switch direction {
    case .north:
      res.y += 1
    case .south:
      res.y -= 1
    case .east:
      res.x += 1
    case .west:
      res.x -= 1
    case .northwest:
      res.y += 1
      res.x -= 1
    case .northeast:
      res.y += 1
      res.x += 1
    case .southwest:
      res.y -= 1
      res.x -= 1
    case .southeast:
      res.y -= 1
      res.x += 1
    case .none:
      break
    }
    
    return res
  }
  
  var nearby: [Point] {
    get {
      let directions: [Direction] = [.north,.south,.east,.west,.northwest,.northeast,.southwest,.southeast]
      return directions.map { move(direction: $0) }
    }
  }
  
  func nearest(check: (Point) -> (Bool), depth: UInt) -> Point? {
    for d in 1...depth {
      for i in (Int(self.x)-Int(d))...(Int(self.x)+Int(d)) {
        let pointAbove = Point(Double(i), Double(self.y+Double(d)))
        let pointBelow = Point(Double(i), Double(self.y-Double(d)))
        if check(pointAbove) { return pointAbove }
        if check(pointBelow) { return pointBelow }
      }
      
      for j in (Int(self.y)-Int(d+1))...(Int(self.y)+Int(d-1)) {
        let pointLeft = Point(self.x-Double(d), Double(j))
        let pointRight = Point(self.x-Double(d), Double(j))
        if check(pointLeft) { return pointLeft }
        if check(pointRight) { return pointRight }
      }
    }
    
    return nil
  }
  
  @discardableResult
  func nearby(check: (Point) -> (Bool), depth: UInt) -> [Point] {
    var points: [Point] = []
    
    for i in (Int(self.x)-Int(depth))...(Int(self.x)+Int(depth)) {
      for j in (Int(self.y)-Int(depth))...(Int(self.y)+Int(depth)) {
        let p = Point(Double(i), Double(j))
        
        if self != p {
          points.append(p)
        }
      }
    }
    
    return points.filter({ return check($0) })
  }
  
  func direction(to point: Point) -> Direction {
    if self.x < point.x {
      if self.y > point.y {
        return .southeast
      } else if self.y < point.y {
        return .northeast
      } else {
        return .east
      }
    } else if self.x > point.x {
      if self.y > point.y {
        return .southwest
      } else if self.y < point.y {
        return .northwest
      } else {
        return .west
      }
    } else {
      if self.y > point.y {
        return .south
      } else if self.y < point.y {
        return .north
      } else {
        return .none
      }
    }
  }
}

extension Point: CustomStringConvertible {
  public var description: String {
    get {
      return "(\(x), \(y))"
    }
  }
}

struct Rect {
  var origin: Point
  var size: Size
  
  public init(size: Size) {
    self.origin = .zero
    self.size = size
  }
  
  public init(origin: Point, size: Size) {
    self.origin = origin
    self.size = size
  }
  
  public init(_ x: Double, _ y: Double, _ width: Double, _ height: Double) {
    self.init(origin: Point(x, y), size: Size(width, height))
  }
  
  var minX: Double {
    return origin.x
  }
  
  var minY: Double {
    return origin.y
  }
  
  var maxX: Double {
    return origin.x + size.width
  }
  
  var maxY: Double {
    return origin.y + size.height
  }
  
  var allPoints: Set<Point> {
    get {
      var res = Set<Point>()
      
      for x in 0..<Int(size.width) {
        for y in 0..<Int(size.height) {
          res.insert(Point(Double(x),Double(y)))
        }
      }
      
      return res
    }
  }
  
  func contains(point: Point) -> Bool {
    return (minX <= point.x && point.x <= maxX) &&
      (minY <= point.y && point.y <= maxY)
  }
  
  var leftTopRect: Rect {
    return Rect(origin: Point(origin.x, origin.y + size.half.height), size: size.half)
  }
  
  var leftBottomRect: Rect {
    return Rect(origin: origin, size: size.half)
  }
  
  var rightTopRect: Rect {
    return Rect(origin: Point(origin.x + size.half.width, origin.y + size.half.height), size: size.half)
  }
  
  var rightBottomRect: Rect {
    return Rect(origin: Point(origin.x + size.half.width, origin.y), size: size.half)
  }
  
  func intersects(rect: Rect) -> Bool {
    return (max(minX, rect.minX) <= min(maxX, rect.maxX)) && (max(minY, rect.minY) <= min(maxY, rect.maxY))
  }
}

extension Rect: CustomStringConvertible {
  public var description: String {
    return "(\(origin), \(size))"
  }
}
