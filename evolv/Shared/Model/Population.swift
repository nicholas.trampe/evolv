//
//  Population.swift
//  evolv iOS
//
//  Created by Nicholas Trampe on 7/16/18.
//  Copyright © 2018 Nicholas Trampe. All rights reserved.
//

import Foundation

struct Individual {
  let tree: ParseTree
  var fitness: Double = 0
  
  init(tree: ParseTree) {
    self.tree = tree
  }
}

enum OrganismTerminal: String {
  case energy = "Energy"
  case thirst = "Thirst"
  
  case closestFoodDistance = "FoodDist"
  case closestWaterDistance = "WaterDist"
  case closestPreyDistance = "PreyDist"
  case closestPredatorDistance = "PredatorDist"
  case closestFriendDistance = "FriendDist"
  
  case averageFoodDistance = "AvgFoodDist"
  case averageWaterDistance = "AvgWaterDist"
  case averagePredatorDistance = "AvgPredatorDist"
  case averagePreyDistance = "AvgPreyDist"
  case averageFriendDistance = "AvgFriendDist"
  
  case averagePredatorEnergy = "AvgPredatorEnergy"
  case averagePreyEnergy = "AvgPreyEnergy"
  case averageFriendEnergy = "AvgFriendEnergy"
  
  case energyOfClosestPredator = "PredatorEnergy"
  case energyOfClosestPrey = "PreyEnergy"
  case energyOfClosestFriend = "FriendEnergy"
  
  case freedom = "Freedom"
  
  static var all: [OrganismTerminal] {
    get {
      return [.energy,
              .thirst,
              .closestFoodDistance,
              .closestWaterDistance,
              .closestPreyDistance,
              .closestPredatorDistance,
              .closestFriendDistance,
              .averageFoodDistance,
              .averageWaterDistance,
              .averagePredatorDistance,
              .averagePreyDistance,
              .averageFriendDistance,
              .averagePredatorEnergy,
              .averagePreyEnergy,
              .averageFriendEnergy,
              .energyOfClosestPredator,
              .energyOfClosestPrey,
              .energyOfClosestFriend,
              .freedom]
    }
  }
  
  static var allValues: [String] {
    get {
      return all.map({ return $0.rawValue })
    }
  }
}

class Population {
  let id: PopulationID
  let config: PopulationConfig
  
  var metrics: PopulationMetrics {
    get {
      let parents = individuals.map({$1})
      let avgFitness = parents.reduce(0) { $0 + $1.fitness } / Double(individuals.count)
      let bestIndividual = (parents.max { $0.fitness < $1.fitness })
      let worstIndividual = (parents.min { $0.fitness < $1.fitness })
      let res = PopulationMetrics(generation: generation,
                                  bestFitness: bestIndividual?.fitness ?? -1,
                                  averageFitness: avgFitness,
                                  worstFitness: worstIndividual?.fitness ?? -1,
                                  bestIndividual: bestIndividual)
      return res
    }
  }
  
  private(set) var individuals: [UInt32:Individual] = [:]
  private(set) var generation: UInt = 0
  private var uniqueId: UInt32 = 0
  
  init(id: PopulationID, config: PopulationConfig) {
    self.id = id
    self.config = config
    
    for _ in 0...self.config.populationSize {
      individuals[uniqueId] = createNewIndividual()
      uniqueId += 1
    }
  }
  
  func removeIndividual(id: UInt32) {
    precondition(individuals[id] != nil, "Cannot find individual")
    
    individuals[id] = nil
  }
  
  func updateIndividualFitness(id: UInt32, fitness: Double) {
    guard let individual = individuals[id] else {
      preconditionFailure("Cannot find individual")
    }
    
    let size = individual.tree.size
    var updatedFitness = fitness
    
    switch config.bloatPreventionMethodType {
    case .none:
      break
    case .parsimonyPressure:
      updatedFitness -= (config.parsimonyPressureCoefficient * Double(size))
      break
    }
    
    if config.preventSimpleSolutions {
      if size < 5 {
        updatedFitness = 0
      }
    }
    
    individuals[id]?.fitness = updatedFitness
  }
  
  // MARK: Population Creation
  
  func createGeneration() {
    let allIndividuals = individuals.map({$1})
    let individualsWithAdjustedFitness = shiftFitnessPositively(choices: allIndividuals)
    let parents = select(choices: individualsWithAdjustedFitness)
    
    print(parents.reduce("", { $0 + String(format: "%.0f", $1.fitness) + ": " + $1.tree.description + "\n"}))
    
    uniqueId = 0
    individuals.removeAll()
    
    for parent in parents {
      individuals[uniqueId] = parent
      uniqueId += 1
    }
    
    while individuals.count < config.populationSize + config.offspringSize {
      individuals[uniqueId] = createIndividual()
      uniqueId += 1
    }
    
    generation += 1
  }
  
  private func createNewIndividual() -> Individual {
    switch self.config.initializationType {
    case .random:
      let tree = ParseTree(type: .random(depth: self.config.maxDepth), terminals: config.terminals.map({$0.rawValue}))
      return Individual(tree: tree)
    case .rampedHalfAndHalf:
      let tree = ParseTree(type: .rampedHalfAndHalf(depth: self.config.maxDepth), terminals: config.terminals.map({$0.rawValue}))
      return Individual(tree: tree)
    }
  }
  
  private func createIndividual() -> Individual {
    let (parent1, parent2) = findParents(choices: individuals.map({$1}))
    var child = crossover(individual1: parent1, individual2: parent2)
    
    mutate(individual: &child)
    
    return child
  }
  
  // MARK: Recombination and Mutation
  
  private func crossover(individual1: Individual, individual2: Individual) -> Individual {
    switch config.crossOverType {
    case .subtree:
      let tree = ParseTree.recombine(tree1: individual1.tree, tree2: individual2.tree)
      return Individual(tree: tree)
    }
  }
  
  private func mutate(individual: inout Individual) {
    switch config.mutationType {
    case .none:
      break
    case .subtree:
      individual.tree.mutate(subtreeDepth: config.mutationSubTreeDepth)
    }
  }
  
  // MARK: Parental Selection
  
  private func findParents(choices: [Individual]) -> (Individual, Individual) {
    switch config.parentSelectionType {
    case .fitnessProportional:
      let p1 = findParentWithFitnessProportional(choices: choices)
      let p2 = findParentWithFitnessProportional(choices: choices)
      return (p1, p2)
    case .overSelection:
      let p1 = findParentWithOverSelection(choices: choices)
      let p2 = findParentWithOverSelection(choices: choices)
      return (p1, p2)
    }
  }
  
  private func findParentWithFitnessProportional(choices: [Individual]) -> Individual {
    let overallFitness: Double = choices.reduce(0) { $0 + $1.fitness }
    var location: Double = 0
    var incrementer: Double = 0
    var index: Int = 0
    
    precondition(overallFitness > 0, "Invalid overall fitness")
    
    location = Double(arc4random() % UInt32(overallFitness))
    
    while incrementer <= location {
      incrementer += choices[index].fitness
      index += 1
    }
    
    return choices[index-1]
  }
  
  private func findParentWithOverSelection(choices: [Individual]) -> Individual {
    let sortedChoices = choices.sorted(by: { return $0.fitness > $1.fitness })
    
    var index: Int = 0
    let lowerThreshold = UInt32(Double(sortedChoices.count)*0.2)
    let upperThreshold = UInt32(Double(sortedChoices.count)*0.8)
    
    if (arc4random() % 2 == 0)
    {
      index = Int(arc4random() % lowerThreshold)
    }
    else
    {
      index = Int((arc4random() % upperThreshold) + lowerThreshold)
    }
    
    return sortedChoices[index]
  }
  
  // MARK: Survivor Selection
  
  private func select(choices: [Individual]) -> [Individual] {
    switch config.survivalSelectionType {
    case .truncation:
      return selectWithTruncation(choices: choices)
    case .tournamentWithoutReplacement:
      return selectWithTournament(choices: choices)
    }
  }
  
  private func selectWithTruncation(choices: [Individual]) -> [Individual] {
    var sortedChoices = choices.sorted(by: { return $0.fitness > $1.fitness })
    
    while sortedChoices.count > config.populationSize {
      _ = sortedChoices.popLast()
    }
    
    return sortedChoices
  }
  
  private func selectWithTournament(choices: [Individual]) -> [Individual] {
    var selectedChoices = choices
    
    while selectedChoices.count > config.populationSize {
      var selectedIndicies: [Int] = []
      
      for _ in 0..<config.survivalSelectionTournamentSize {
        selectedIndicies.append(Int(arc4random() % UInt32(selectedChoices.count)))
      }
      
      selectedIndicies.sort { (i1, i2) -> Bool in
        selectedChoices[i1].fitness > selectedChoices[i2].fitness
      }
      
      selectedChoices.remove(at: selectedIndicies.last!)
    }
    
    return selectedChoices
  }
  
  // MARK: Parsimony Pressure Fitness Adjustment
  
  func shiftFitnessPositively(choices: [Individual]) -> [Individual] {
    if let minFitness = choices.min(by: { return $0.fitness < $1.fitness })?.fitness {
      return choices.map({
        var ind = $0
        ind.fitness += fabs(minFitness) + 1
        return ind
      })
    }
    
    return choices
  }
}

extension Population: GameController {
  func didUpdate(organism: Organism) {
    let fitness = max(Double(organism.energy), 1) - Double(organism.thirst)
    updateIndividualFitness(id: organism.id, fitness: fitness)
  }
  
  func nextAction(for organism: Organism, state: GameState) -> Action? {
    let actions = state.possibleActionsForOrganism(at: organism.location)
    let fitnesses = actions.map { fitness(for: organism, action: $0, in: state) }
    
    if let maxFitness = fitnesses.max() {
      if let index = fitnesses.firstIndex(of: maxFitness) {
        return actions[index]
      }
    }
    
    return nil
  }
  
  private func fitness(for organism: Organism, action: Action, in state: GameState) -> Double {
    var updatedState = state
    let updatedOrganism = updatedState.perform(action: action).actingOrganism
    
    guard let tree = individuals[updatedOrganism.id]?.tree else {
      preconditionFailure("Individual does not exist")
    }
    
    let maximumDistance: Double = 15
    let energy = Double(updatedOrganism.energy)
    let thirst = Double(updatedOrganism.thirst)
    
    var allFoodPoints: [Point] = []
    var closestFoodPoint: Point? = nil
    
    var allWaterPoints: [Point] = []
    var closestWaterPoint: Point? = nil
    
    var allPrey: [Organism] = []
    var closestPrey: Organism? = nil
    var allPredators: [Organism] = []
    var closestPredator: Organism? = nil
    var allFriends: [Organism] = []
    var closestFriend: Organism? = nil
    var averagePreyDistance: Double = 0
    var averagePreyEnergy: Double = 0
    var averagePredatorDistance: Double = 0
    var averagePredatorEnergy: Double = 0
    var averageFriendDistance: Double = 0
    var averageFriendEnergy: Double = 0
    
    let freedom: Double = Double(updatedState.possibleActionsForOrganism(at: updatedOrganism.location).count)
    
    updatedState.iterateOrganisms { (org) in
      let distance = org.location.distance(from: updatedOrganism.location)
      let energy = Double(energy)
      
      if distance > maximumDistance {
        return
      }
      
      if updatedOrganism.isPrey(potentialPrey: org) {
        allPrey.append(org)
        averagePreyEnergy += energy
        averagePreyDistance += distance
        
        let closestPreyDistance = closestPrey?.location.distance(from: updatedOrganism.location) ?? Double.greatestFiniteMagnitude
        
        if distance < closestPreyDistance {
          closestPrey = org
        }
      } else if updatedOrganism.isPredator(potentialPredator: org) {
        allPredators.append(org)
        averagePredatorEnergy += energy
        averagePredatorDistance += distance
        
        let closestPredatorDistance = closestPredator?.location.distance(from: updatedOrganism.location) ?? Double.greatestFiniteMagnitude
        
        if distance < closestPredatorDistance {
          closestPredator = org
        }
      } else {
        allFriends.append(org)
        averageFriendEnergy += energy
        averageFriendDistance += distance
        
        let closestFriendDistance = closestFriend?.location.distance(from: updatedOrganism.location) ?? Double.greatestFiniteMagnitude
        
        if distance < closestFriendDistance {
          closestFriend = org
        }
      }
    }
    
    updatedState.iterateItems { item in
      let distance = item.location.distance(from: updatedOrganism.location)
      
      if distance > maximumDistance {
        return
      }
      
      switch item.type {
      case .food:
        allFoodPoints.append(item.location)
        
        let closestFoodDistance = closestFoodPoint?.distance(from: updatedOrganism.location) ?? Double.greatestFiniteMagnitude
        
        if distance < closestFoodDistance {
          closestFoodPoint = item.location
        }
      case .water:
        allWaterPoints.append(item.location)
        
        let closestWaterDistance = closestWaterPoint?.distance(from: updatedOrganism.location) ?? Double.greatestFiniteMagnitude
        
        if distance < closestWaterDistance {
          closestWaterPoint = item.location
        }
      case .wall:
        break
      }
    }
    
    let foodDistance: Double = closestFoodPoint?.distance(from: updatedOrganism.location) ?? Double.greatestFiniteMagnitude
    let averageFoodDistance: Double = updatedOrganism.location.distance(from: allFoodPoints)
    
    let waterDistance: Double = closestWaterPoint?.distance(from: updatedOrganism.location) ?? Double.greatestFiniteMagnitude
    let averageWaterDistance: Double = updatedOrganism.location.distance(from: allWaterPoints)
    
    let closestPreyDistance: Double = closestPrey?.location.distance(from: updatedOrganism.location) ?? Double.greatestFiniteMagnitude
    let closestPreyEnergy: Double = Double(closestPrey?.energy ?? 0)
    
    let closestPredatorDistance: Double = closestPredator?.location.distance(from: updatedOrganism.location) ?? Double.greatestFiniteMagnitude
    let closestPredatorEnergy: Double = Double(closestPredator?.energy ?? 0)
    
    let closestFriendDistance: Double = closestFriend?.location.distance(from: updatedOrganism.location) ?? Double.greatestFiniteMagnitude
    let closestFriendEnergy: Double = Double(closestFriend?.energy ?? 0)
    
    
    let value = tree.value({ (op) -> (Double) in
      guard let terminal = OrganismTerminal(rawValue: op) else {
        preconditionFailure("Unable to create terminal node from string value")
      } 
      
      switch terminal {
      case .energy:
        return energy
      case .thirst:
        return thirst 
      case .closestFoodDistance:
        return foodDistance
      case .closestWaterDistance:
        return waterDistance
      case .closestPreyDistance:
        return closestPreyDistance
      case .closestPredatorDistance:
        return closestPredatorDistance
      case .closestFriendDistance:
        return closestFriendDistance
      case .averageFoodDistance:
        return averageFoodDistance
      case .averageWaterDistance:
        return averageWaterDistance
      case .averagePredatorDistance:
        return averagePredatorDistance
      case .averagePreyDistance:
        return averagePreyDistance
      case .averageFriendDistance:
        return averageFriendDistance
      case .averagePredatorEnergy:
        return averagePredatorEnergy
      case .averagePreyEnergy:
        return averagePreyEnergy
      case .averageFriendEnergy:
        return averageFriendEnergy
      case .energyOfClosestPredator:
        return closestPredatorEnergy
      case .energyOfClosestPrey:
        return closestPreyEnergy
      case .energyOfClosestFriend:
        return closestFriendEnergy
      case .freedom:
        return freedom
      }
    })
    
    return value
  }
}
