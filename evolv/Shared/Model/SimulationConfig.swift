//
//  SimulationConfig.swift
//  evolv
//
//  Created by Nicholas Trampe on 7/20/18.
//  Copyright © 2018 Nicholas Trampe. All rights reserved.
//

import Foundation

struct SimulationTerminationOptions: OptionSet {
  let rawValue: UInt8
  
  static let bestFitnessConverged = SimulationTerminationOptions(rawValue: 1 << 0)
  static let maximumNumberOfEvaluations = SimulationTerminationOptions(rawValue: 1 << 1)
  
  static let all: SimulationTerminationOptions = [.bestFitnessConverged, .maximumNumberOfEvaluations]
}

struct SimulationConfig {
  let terminationOptions: SimulationTerminationOptions = []
  let maximumNumberOfEvaluations: UInt64 = 1000000
  
  let populations = [PopulationID:PopulationConfig](dictionaryLiteral:
    (.pip, PopulationConfig(populationSize: 50,
                            offspringSize: 50,
                            maxDepth: 6,
                            terminals: [.energy,
                                        .thirst,
                                        .closestFoodDistance,
                                        .closestWaterDistance,
                                        .closestPredatorDistance,
                                        .closestFriendDistance,
                                        .averageFoodDistance,
                                        .averageWaterDistance,
                                        .averagePredatorDistance,
                                        .averageFriendDistance,
                                        .averagePredatorEnergy,
                                        .averageFriendEnergy,
                                        .energyOfClosestPredator,
                                        .energyOfClosestFriend,
                                        .freedom],
                            attackMask: .none,
                            canEatFood: true,
                            initializationType: .rampedHalfAndHalf,
                            parentSelectionType: .overSelection,
                            survivalSelectionType: .tournamentWithoutReplacement,
                            crossOverType: .subtree,
                            mutationType: .subtree,
                            bloatPreventionMethodType: .parsimonyPressure,
                            preventSimpleSolutions: true,
                            survivalSelectionTournamentSize: 5,
                            mutationSubTreeDepth: 4,
                            parsimonyPressureCoefficient: 0.1)),
    (.chip, PopulationConfig(populationSize: 20,
                              offspringSize: 20,
                              maxDepth: 6,
                              terminals: [.energy,
                                          .thirst,
                                          .closestWaterDistance,
                                          .closestPreyDistance,
                                          .averageWaterDistance,
                                          .averagePreyDistance,
                                          .averagePreyEnergy,
                                          .energyOfClosestPrey,
                                          .freedom],
                              attackMask: [.pip],
                              canEatFood: false,
                              initializationType: .rampedHalfAndHalf,
                              parentSelectionType: .overSelection,
                              survivalSelectionType: .tournamentWithoutReplacement,
                              crossOverType: .subtree,
                              mutationType: .subtree,
                              bloatPreventionMethodType: .parsimonyPressure,
                              preventSimpleSolutions: true,
                              survivalSelectionTournamentSize: 5,
                              mutationSubTreeDepth: 4,
                              parsimonyPressureCoefficient: 0.1)))
  
  
  let game: GameConfig = GameConfig(size: Size(30,30),
                                    moveEnergyLoss: 6,
                                    moveThirstGain: 1,
                                    attackEnergyGainCoefficient: 0.5,
                                    attackThirstGain: 1,
                                    eatThirstGain: 1,
                                    restEnergyLoss: 5,
                                    restThirstGain: 1,
                                    drinkThirstLoss: 20,
                                    drinkEnergyLoss: 5,
                                    timeLimit: 200,
                                    minimumNumberOfSurvivingOrganisms: 10,
                                    foodEnergy: 25,
                                    foodDensity: 0.2,
                                    startingOrganismEnergy: 25)
  
  
  func predators(for prey: PopulationID) -> PopulationIDMask {
    var res: PopulationIDMask = .none
    
    for (predatorID, population) in populations {
      if population.attackMask.contains(PopulationIDMask(rawValue: prey.rawValue)) {
        res.insert(PopulationIDMask(rawValue: predatorID.rawValue))
      }
    }
    
    return res
  }
}
