//
//  Bridging.swift
//  evolv macOS
//
//  Created by Nicholas Trampe on 7/17/18.
//  Copyright © 2018 Nicholas Trampe. All rights reserved.
//

import Foundation
import SpriteKit

#if os(OSX)

import AppKit

public typealias ViewType = NSView
public typealias SpriteViewType = SKView
public typealias Label = NSText
public typealias Button = NSButton
public typealias StackView = NSStackView
public typealias Color = NSColor
public typealias Font = NSFont
public typealias LayoutConstraint = NSLayoutConstraint

extension NSColor {
  public convenience init(red: CGFloat, green: CGFloat, blue: CGFloat, alpha: CGFloat) {
    self.init(srgbRed: red, green: green, blue: blue, alpha: alpha)
  }
}

extension NSText {
  public var text: String? {
    get {
      return string
    }
    
    set {
      string = newValue ?? ""
    }
  }
  
  public var numberOfLines: Int {
    get {
      return 0
    }
    set {
      
    }
  }
  
  public var textAlignment: NSTextAlignment {
    get {
      return alignment
    }
    set {
      alignment = newValue
    }
  }
}

extension NSButton {
  func addTarget(_ target: AnyObject?, action: Selector) {
    self.target = target
    self.action = action
  }
}

extension NSView {
  var color: NSColor? {
    get {
      if let cg = layer?.backgroundColor {
        return NSColor(cgColor: cg)
      }
      
      return nil
    }
    
    set {
      wantsLayer = true
      layer?.backgroundColor = newValue?.cgColor
    }
  }
  
  var cornerRadius: CGFloat {
    get {
      return layer?.cornerRadius ?? 0
    }
    
    set {
      layer?.cornerRadius = newValue
    }
  }
}

#else

import UIKit

public typealias ViewType = UIView
public typealias SpriteViewType = SKView
public typealias Label = UITextView
public typealias Button = UIButton
public typealias StackView = UIStackView
public typealias Color = UIColor
public typealias Font = UIFont
public typealias LayoutConstraint = NSLayoutConstraint

extension UIView {
  var color: UIColor? {
    get {
      return backgroundColor
    }
    
    set {
      backgroundColor = newValue
    }
  }
  
  var cornerRadius: CGFloat {
    get {
      return layer.cornerRadius
    }
    
    set {
      layer.cornerRadius = newValue
    }
  }
}

extension UIButton {
  convenience init(title: String, target: Any?, action: Selector?) {
    self.init(frame: .zero)
    
    if let action = action {
      addTarget(target, action: action, for: .touchUpInside)
    }
    
    setTitle(title, for: .normal)
  }
  
  var font: Font? {
    get {
      return titleLabel?.font
    }
    
    set {
      titleLabel?.font = newValue
    }
  }
  
  var title: String {
    get {
      return title(for: .normal) ?? ""
    }
    
    set {
      setTitle(newValue, for: .normal)
    }
  }
  
  func addTarget(_ target: Any?, action: Selector) {
    addTarget(target, action: action, for: .touchUpInside)
  }
}

#endif

open class View: ViewType {
  
  #if os(OSX)
  
  open var userInteractionEnabled: Bool {
    return true
  }
  
  open override func viewDidMoveToWindow() {
    didMoveToWindow()
  }
  
  open func didMoveToWindow() {
    super.viewDidMoveToWindow()
  }
  
  open override func viewDidMoveToSuperview() {
    didMoveToSuperview()
  }
  
  open func didMoveToSuperview() {
    super.viewDidMoveToSuperview()
  }
  
  #endif
  
}

open class SpriteView: SpriteViewType {
  
  #if os(OSX)
  
  open var userInteractionEnabled: Bool {
    return true
  }
  
  open override func viewDidMoveToWindow() {
    didMoveToWindow()
  }
  
  open func didMoveToWindow() {
    super.viewDidMoveToWindow()
  }
  
  open override func viewDidMoveToSuperview() {
    didMoveToSuperview()
  }
  
  open func didMoveToSuperview() {
    super.viewDidMoveToSuperview()
  }
  
  #endif
  
}
