//
//  ControlView.swift
//  evolv
//
//  Created by Nicholas Trampe on 7/17/18.
//  Copyright © 2018 Nicholas Trampe. All rights reserved.
//

import Foundation

typealias ControlViewAction = () -> ()

class ControlView: View {
  private let stack = StackView(frame: .zero)
  private var actions: [ControlViewAction] = []
  
  init() {
    super.init(frame: .zero)
    
    configureViews()
    constrainViews()
  }
  
  required init?(coder decoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  // Public API
  
  func add(icon: IconFontIcon, at index: Int, action: @escaping ControlViewAction) {
    let button = Button.iconButton(of: 32, with: icon)
    button.translatesAutoresizingMaskIntoConstraints = false
    button.tag = index
    button.addTarget(self, action: #selector(buttonPressed))
    actions.append(action)
    stack.addArrangedSubview(button)
  }
  
  func update(icon: IconFontIcon, at index: Int) {
    if let button = stack.arrangedSubviews.filter({$0.tag == index}).first as? Button {
      button.icon = icon
    }
  }
  
  // Private API
  
  private func configureViews() {
    color = Color.black.withAlphaComponent(0.8)
    cornerRadius = 15
    
    stack.translatesAutoresizingMaskIntoConstraints = false
    stack.spacing = 10
    addSubview(stack)
  }
  
  private func constrainViews() {
    LayoutConstraint.activate([
      stack.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 20),
      stack.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -20),
      stack.topAnchor.constraint(equalTo: self.topAnchor, constant: 20),
      stack.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -20),
      ])
  }
  
  @objc private func buttonPressed(sender: Button) {
    actions[sender.tag]()
  }
}
