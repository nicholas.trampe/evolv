//
//  GameView.swift
//  evolv iOS
//
//  Created by Nicholas Trampe on 7/17/18.
//  Copyright © 2018 Nicholas Trampe. All rights reserved.
//

import SpriteKit

class GameView: SpriteView, SimulationView {
  var animationInterval: Double = 0
  
  weak var observer: SimulationViewObserver? {
    get {
      return simulation.observer
    }
    set {
      simulation.observer = newValue
    }
  }
  var size: CGSize {
    get {
      return simulation.size
    }
  }
  var cameraPosition: CGPoint {
    get {
      guard let camera = simulation.camera else {
        return .zero
      }
      
      return camera.position
    }
  }
  var cameraScale: CGFloat {
    get {
      guard let camera = simulation.camera else {
        return 0
      }
      
      return camera.xScale
    }
  }
  
  private let simulation: GameScene = GameScene()
  private let controlView = ControlView()
  
  init() {
    super.init(frame: .zero)
    
    ignoresSiblingOrder = true
    
    configureViews()
    constrainViews()
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("meh")
  }
  
  override func didMoveToSuperview() {
    presentScene(simulation)
  }
  
  // MARK: Public API
  
  // MARK: Adding / Removing
  
  func addNode(id: NodeID, position: CGPoint, zPosition: CGFloat, texture: SKTexture, scale: CGFloat) {
    let node = SKSpriteNode(texture: texture)
    node.position = position
    node.name = "\(id)"
    node.zPosition = zPosition
    node.setScale(scale)
    simulation.nodes[id] = node
    simulation.addChild(node)
  }
  
  func removeNode(id: NodeID) {
    if let node = simulation.nodes[id] {
      node.removeAllActions()
      node.removeFromParent()
      simulation.nodes[id] = nil
    }
  }
  
  func removeAllNodes() {
    simulation.nodes.forEach({ $1.removeAllActions(); $1.removeFromParent() })
    simulation.nodes.removeAll()
  }
  
  // MARK: Accessors
  
  func location(for id: NodeID) -> CGPoint? {
    return simulation.nodes[id]?.position
  }
  
  func xScale(for id: NodeID) -> CGFloat? {
    return simulation.nodes[id]?.xScale
  }
  
  func yScale(for id: NodeID) -> CGFloat? {
    return simulation.nodes[id]?.yScale
  }
  
  // MARK: Animating Nodes
  
  func animateNode(id: NodeID, action: SKAction) {
    animateNode(id: id, action: action) {}
  }
  
  func animateNode(id: NodeID, action: SKAction, completion block: @escaping () -> Void) {
    guard let node = simulation.nodes[id] else {
      return
    }
    
    let complete = SKAction.run {
      block()
    }
    
    let sequence = SKAction.sequence([action, complete])
    node.run(sequence, withKey: "NodeCustomAnimation")
  }
  
  func animateNode(id: NodeID, textures: [SKTexture]) {
    animateNode(id: id, textures: textures) {}
  }
  
  func animateNode(id: NodeID, textures: [SKTexture], completion block: @escaping () -> Void) {
    guard let node = simulation.nodes[id] else {
      return
    }
    
    let action = SKAction.animate(with: textures,
                                  timePerFrame: animationInterval / Double(textures.count),
                                  resize: false,
                                  restore: false)
    
    let complete = SKAction.run {
      block()
    }
    
    let sequence = SKAction.sequence([action, complete])
    node.run(sequence, withKey: "NodeTextureAnimation")
  }
  
  func moveNode(id: NodeID, position: CGPoint) {
    moveNode(id: id, position: position) {}
  }
  
  func moveNode(id: NodeID, position: CGPoint, completion block: @escaping () -> Void) {
    guard let node = simulation.nodes[id] else {
      return
    }
    
    let positionAction = SKAction.move(to: position, duration: animationInterval)
    let complete = SKAction.run {
      block()
    }
    let sequence = SKAction.sequence([positionAction, complete])
    node.run(sequence, withKey: "NodeMoveAnimation")
  }
  
  func scaleNode(id: NodeID, scaleX: CGFloat, scaleY: CGFloat, animated: Bool) {
    scaleNode(id: id, scaleX: scaleX, scaleY: scaleY, animated: animated) {}
  }
  
  func scaleNode(id: NodeID, scaleX: CGFloat, scaleY: CGFloat, animated: Bool, completion block: @escaping () -> Void) {
    guard let node = simulation.nodes[id] else {
      return
    }
    
    let scaleAction = SKAction.scaleX(to: scaleX, y: scaleY, duration: animated == true ? animationInterval : 0)
    let complete = SKAction.run {
      block()
    }
    let sequence = SKAction.sequence([scaleAction, complete])
    node.run(sequence, withKey: "NodeScaleAnimation")
  }
  
  func flashNode(id: NodeID) {
    flashNode(id: id) {}
  }
  
  func flashNode(id: NodeID, completion block: @escaping () -> Void) {
    guard let node = simulation.nodes[id] else {
      return
    }
    
    let flashAction = SKAction.repeat(SKAction.flash(duration: animationInterval * 0.25), count: 2)
    let complete = SKAction.run {
      block()
    }
    let sequence = SKAction.sequence([flashAction, complete])
    node.run(sequence, withKey: "NodeFlashAnimation")
  }
  
  func oscillateNode(id: NodeID, in direction: Direction, distances: [CGFloat], weights: [Double]) {
    oscillateNode(id: id, in: direction, distances: distances, weights: weights) {}
  }
  
  func oscillateNode(id: NodeID, in direction: Direction, distances: [CGFloat], weights: [Double], completion block: @escaping () -> Void) {
    guard let node = simulation.nodes[id] else {
      return
    }
    
    let moveAction = SKAction.oscillate(in: direction,
                                        distances: distances,
                                        weights: weights,
                                        duration: animationInterval)
    let complete = SKAction.run {
      block()
    }
    let sequence = SKAction.sequence([moveAction, complete])
    node.run(sequence, withKey: "NodeOscillateAnimation")
  }
  
  // MARK: Camera Animations
  
  func move(to position: CGPoint, duration: Double) {
    guard let camera = simulation.camera else {
      return
    }
    
    if duration > 0 {
      let moveAction = SKAction.move(to: position, duration: duration)
      camera.run(moveAction, withKey: "CameraMoveAnimation")
    } else {
      camera.position = position
    }
  }
  
  func zoom(to scale: CGFloat, duration: Double) {
    guard let camera = simulation.camera else {
      return
    }
    
    if duration > 0 {
      let zoomAction = SKAction.scale(to: scale, duration: duration)
      camera.run(zoomAction, withKey: "CameraZoomAnimation")
    } else {
      camera.setScale(scale)
    }
  }
  
  // MARK: Debug
  
  func setText(id: UInt32, text: String?, at position: CGPoint) {
    simulation.setText(id: id, text: text, at: position)
  }
  
  func removeText(id: UInt32) {
    simulation.removeText(id: id)
  }
  
  // MARK: Controls

  func addControl(icon: IconFontIcon, at index: Int) {
    controlView.add(icon: icon, at: index) { 
      self.observer?.didPressControl(at: index)
    }
  }
  
  func updateControl(icon: IconFontIcon, at index: Int) {
    controlView.update(icon: icon, at: index)
  }
  
  // MARK: Tile Map
  
  func addTileMap(id: UInt32, named: String, columns: Int, rows: Int, tileSize: CGSize) {
    simulation.addTileMap(id: id, named: named, columns: columns, rows: rows, tileSize: tileSize)
  }
  
  func setTileFill(id: UInt32, named: String) {
    simulation.setTileFill(id: id, named: named)
  }
  
  func setTileFill(id: UInt32, named: String, column: Int, row: Int) {
    simulation.setTileFill(id: id, named: named, column: column, row: row)
  }
  
  func clearTiles(id: UInt32) {
    simulation.clearTiles(id: id)
  }
  
  // MARK: Private API
  
  private func configureViews() {
    controlView.translatesAutoresizingMaskIntoConstraints = false
    addSubview(controlView)
  }
  
  private func constrainViews() {
    LayoutConstraint.activate([
      controlView.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -20),
      controlView.centerXAnchor.constraint(equalTo: self.centerXAnchor),
      ])
  }
}

private class GameScene: SKScene {
  weak var observer: SimulationViewObserver? = nil
  var nodes: [NodeID:SKNode] = [:]
  var texts: [UInt32:SKLabelNode] = [:]
  var maps: [UInt32:SKTileMapNode] = [:]
  
  override init() {
    super.init(size: CGSize(width: 500, height: 500))
    
    scaleMode = .aspectFill
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  func setUpScene() {
    
    let cameraNode = SKCameraNode()
    
    cameraNode.position = CGPoint(x: self.frame.midX, y: self.frame.midY)
    addChild(cameraNode)
    camera = cameraNode
  }
  
  override func didMove(to view: SKView) {
    self.setUpScene()
    self.setUpControls()
  }
  
  func setText(id: UInt32, text: String?, at position: CGPoint) {
    if texts[id] == nil {
      let node = SKLabelNode(fontNamed: "Helvetica")
      node.position = position
      node.numberOfLines = 0
      node.zPosition = 2
      node.fontSize = 64
      addChild(node)
      
      texts[id] = node
    }
    
    if let node = texts[id] {
      node.text = text
      node.position = CGPoint(x: node.frame.size.width / 2.0 + position.x, y:  position.y)
    }
  }
  
  func removeText(id: UInt32) {
    if let node = texts[id] {
      node.removeAllActions()
      node.removeFromParent()
      texts[id] = nil
    }
  }
  
  func addTileMap(id: UInt32, named: String, columns: Int, rows: Int, tileSize: CGSize) {
    guard let tileSet = SKTileSet(named: named) else {
      preconditionFailure("Tileset not found")
    }
    
    let map = SKTileMapNode(tileSet: tileSet, columns: columns, rows: rows, tileSize: tileSize)
    map.name = named
    map.position = CGPoint(x: -tileSize.width / 2.0, y: -tileSize.height / 2.0)
    map.anchorPoint = CGPoint(x: 0, y: 0)
    map.enableAutomapping = true
    map.zPosition = -100 + CGFloat(maps.count)
    addChild(map)
    maps[id] = map
  }
  
  func setTileFill(id: UInt32, named: String) {
    guard let map = maps[id] else {
      preconditionFailure("Map not found")
    }
    
    let tileGroup = map.tileSet.tileGroups.first(where: {$0.name == named})
    map.fill(with: tileGroup)
  }
  
  func setTileFill(id: UInt32, named: String, column: Int, row: Int) {
    guard let map = maps[id] else {
      preconditionFailure("Map not found")
    }
    
    let tileGroup = map.tileSet.tileGroups.first(where: {$0.name == named})
    map.setTileGroup(tileGroup, forColumn: column, row: row)
  }
  
  func clearTiles(id: UInt32) {
    guard let map = maps[id] else {
      preconditionFailure("Map not found")
    }
    
    map.fill(with: nil)
  }
}

#if os(iOS)
// Touch-based event handling
extension GameScene {
  
  func setUpControls() {
    let pinchGesture = UIPinchGestureRecognizer()
    pinchGesture.addTarget(self, action: #selector(pinchGestureAction(_:)))
    view?.addGestureRecognizer(pinchGesture)
    
    let panGesture = UIPanGestureRecognizer()
    panGesture.addTarget(self, action: #selector(panGestureAction(_:)))
    view?.addGestureRecognizer(panGesture)
  }
  
  @objc func panGestureAction(_ sender: UIPanGestureRecognizer) {
    guard let camera = self.camera else {
      return
    }
    
    if sender.state == .began {
      observer?.didBeginPanning(at: camera.position)
    }
    
    let translation = sender.translation(in: view)
    observer?.didPan(distance: CGPoint(x: translation.x, y: -translation.y))
  }
  
  @objc func pinchGestureAction(_ sender: UIPinchGestureRecognizer) {
    guard let camera = self.camera else {
      return
    }
    
    if sender.state == .began {
      observer?.didBeginZooming(at: camera.xScale)
    }
    
    observer?.didZoom(scale: sender.scale)
  }
  
  override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
    
  }
  
  override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
    
  }
  
  override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
    for t in touches {
      let location = t.location(in: self)
      observer?.didTap(point: location)
    }
  }
  
  override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
    
  }
  
  
}
#endif

#if os(OSX)
// Mouse-based event handling

extension SKView {
  open override func scrollWheel(with event: NSEvent) {
    scene?.scrollWheel(with: event)
  }
  
  open override func magnify(with event: NSEvent) {
    scene?.magnify(with: event)
  }
}

extension GameScene {
  
  func setUpControls() {
    let panGesture = NSPanGestureRecognizer(target: self, action: #selector(panGestureAction(_:)))
    view?.addGestureRecognizer(panGesture)
  }
  
  @objc func panGestureAction(_ sender: NSPanGestureRecognizer) {
    guard let camera = self.camera else {
      return
    }
    
    if sender.state == .began {
      observer?.didBeginPanning(at: camera.position)
    }
    
    let translation = sender.translation(in: view)
    observer?.didPan(distance: translation)
  }
  
  override func mouseDown(with event: NSEvent) {
    let location = event.location(in: self)
    observer?.didTap(point: location)
  }
  
  override func mouseDragged(with event: NSEvent) {
    
  }
  
  override func mouseUp(with event: NSEvent) {
    
  }
  
  override func scrollWheel(with event: NSEvent) {
    let delta = event.scrollingDeltaY
    observer?.didZoom(delta: delta)
  }
  
  override func magnify(with event: NSEvent) {
    let delta = event.deltaZ
    observer?.didZoom(delta: delta)
  }
  
}
#endif

