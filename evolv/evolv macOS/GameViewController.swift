//
//  GameViewController.swift
//  evolv macOS
//
//  Created by Nicholas Trampe on 7/9/18.
//  Copyright © 2018 Nicholas Trampe. All rights reserved.
//

import Cocoa
import SpriteKit
import GameplayKit

class GameViewController: NSViewController {
  
  private let simulationModel = Simulation(config: SimulationConfig())
  private let simulationView = GameView()
  private var presenter: SimulationPresenter? = nil
  
  override func loadView() {
    view = simulationView
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    presenter = EVSimulationPresenter(view: simulationView, model: simulationModel)
  }
  
  override func viewDidAppear() {
    super.viewDidAppear()
    presenter?.start()
  }
}

