//
//  IconButton.swift
//  evolv macOS
//
//  Created by Nicholas Trampe on 7/17/18.
//  Copyright © 2018 Nicholas Trampe. All rights reserved.
//

import AppKit

class IconButton: NSButton {
  override func draw(_ dirtyRect: NSRect) {
    let textRect = NSRect(x: 0, y: 0, width: self.frame.width, height: self.frame.height)
    let textTextContent = self.title
    let textStyle = NSMutableParagraphStyle()
    textStyle.alignment = .center
    
    let textFontAttributes: [NSAttributedString.Key:Any] = [NSAttributedString.Key.font: self.font!,
                                                           NSAttributedString.Key.foregroundColor: NSColor.white,
                                                           NSAttributedString.Key.paragraphStyle: textStyle]

    NSGraphicsContext.saveGraphicsState()
    textTextContent.draw(in: textRect, withAttributes: textFontAttributes)
    NSGraphicsContext.restoreGraphicsState()
  }
}
