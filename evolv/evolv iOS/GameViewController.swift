//
//  GameViewController.swift
//  evolv iOS
//
//  Created by Nicholas Trampe on 7/9/18.
//  Copyright © 2018 Nicholas Trampe. All rights reserved.
//

import UIKit
import SpriteKit
import GameplayKit

class GameViewController: UIViewController {
  
  private let simulationModel = Simulation(config: SimulationConfig())
  private let simulationView = GameView()
  private var presenter: SimulationPresenter? = nil
  
  override func loadView() {
    view = simulationView
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    presenter = EVSimulationPresenter(view: simulationView, model: simulationModel)
  }
  
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    presenter?.start()
  }
  
  override var shouldAutorotate: Bool {
    return true
  }
  
  override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
    if UIDevice.current.userInterfaceIdiom == .phone {
      return .allButUpsideDown
    } else {
      return .all
    }
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Release any cached data, images, etc that aren't in use.
  }
  
  override var prefersStatusBarHidden: Bool {
    return true
  }
}
